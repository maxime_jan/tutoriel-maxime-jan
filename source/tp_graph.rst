TP : Générer un graphique avec les données des capteurs
#######################################################

Direction à prendre
*******************

Vous avez maintenant toutes les cartes en main pour générer un graphique avec les données récupérées dans le dernier TP. Voyons les différentes étapes de ce travail :

* Convertir les dates (actuellement en millisecondes) de la liste de données en objets ``Date``.
* Différencier tous les cas de ``values`` (reprenez le code du TP précédent).
* Pour chaque cas, modifier la propriété ``colors`` de l'objet ``options`` qui change la couleur des lignes du graphique. Celle-ci est une liste de ``string`` contenant des couleurs en notation héxadécimal.
* Pour chaque cas, modifier la propriété ``title`` de l'objet ``options`` qui ajoutera un titre au graphique.
* Pour chaque cas, modifier la propriété ``vAxes`` de l'objet ``options`` qui ajoutera un titre aux axes Y. Celui-ci prend cette forme ``classicOptions.vAxes = { 0: {title: "titre 1"}, 1: {title: "titre 2"} }``.
* Pour chaque cas, ajouter les bonnes colonnes.
* Insérer ``dataList`` dans la fonction ``addRow``.

Bon travail !

Corrections
***********

Il est temps de passer à la correction. Nous allons commencer par la conversion de la date. Notez que tous les extraits de code qui suivent se trouvent à l'intérieur de la fonction ``drawChart``.

.. captionup:: graph.js
.. code-block:: js

    for (var i = 0; i < dataList.length; i++) {
        dataList[i][0] = new Date(dataList[i][0]);
    }

Puis, afin de gérer tous les cas de manière efficace, il est plus aisé de créer des variables pour les couleurs, les titres des axes Y et le titre du graphique.

.. captionup:: graph.js
.. code-block:: js
    
    color = [];
    title1 = "";
    title2 = "";
    mainTitle = "";
    
On peut ensuite reprendre la même structure conditionnelle que dans le fichier ``dataHandler.js`` pour différencier tous les cas de ``values``. Pour chacun de ces cas, on ajoute les titres et les couleurs correspondantes.


.. captionup:: graph.js
.. code-block:: js

    if (include(values, "Température") && include(values, "Humidité")) {
        title1 = "Température (C)";
        title2 = "Humidité relative (%)";
        color.push('#ff0000');
        color.push('#006bff');
        mainTitle = "Evolution de la tempértaure et de l'humidité relative"
    } else if (include(values, "Température") && include(values, "Pression")) {
        title1 = "Température (C)";
        title2 = "Pression atmosphérique (hPa)";
        color.push('#ff0000');
        color.push('#b89502');
        mainTitle = "Evolution de la tempértaure et de la pression atmosphérique"
    } else if (include(values, "Humidité") && include(values, "Pression")) {
        title1 = "Humidité relative (%)";
        title2 = "Pression atmosphérique (hPa)";
        color.push('#006bff');
        color.push('#b89502');
        mainTitle = "Evolution de l'humidité relative et de la pression atmosphérique"
    } else if (include(values, "Humidité")) {
        title1 = "Humidité relative (%)";
        color.push('#006bff');
        mainTitle = "Evolution de l'humidité relative";
    } else if (include(values, "Température")) {
        title1 = "Température (C)";
        color.push('#ff0000');
        mainTitle = "Evolution de la tempértaure";
    } else {
        title1 = "Pression atmosphérique (hPa)";
        color.push('#b89502');
        mainTitle = "Evolution de la pression atmosphérique";
    }

Puis, on peut modifier les propriétés de ``options``. La colonne de l'axe X peut également être implantée.

.. captionup:: graph.js
.. code-block:: js
    
    options.colors = color;
    options.title = mainTitle;
    
    dataTable.addColumn('date', 'Temps');
    
Pour l'ajout de colonnes et de titres de l'axe Y, il faut contrôler si l'on doit en ajouter un ou deux de chaque. Pour cela, on regarde s'il y a quelque chose dans la variable 
``title2``. Une fois cela fait, il suffit de simplement modifier ``vAxes`` et d'utiliser ``addColumn``.

.. captionup:: graph.js
.. code-block:: js
    
    if (title2) {
        classicOptions.vAxes = {
            0: {
                title: title1
            }
            1: {
                title: title2
            }
        }
        dataTable.addColumn('number', title1);
        dataTable.addColumn('number', title2);
    } else {
        classicOptions.vAxes = {
            0: {
                title: title1
            }
        }
        dataTable.addColumn('number', title1);
    }
    
Il ne reste plus qu'à ajouter les données de la liste ``dataList`` à ``dataTable``.

.. captionup:: graph.js
.. code-block:: js
    
    data.addRows(dataList);
    
Avec ceci, les graphiques se génèrent maintenant correctement en fonction des types de valeurs cochés lorsque vous appuyez sur le bouton.

Mettre à jour le graphique en temps réel
****************************************

Comme vous avez pu le voir lors de la vidéo de présentation, il est possible de modifier le graphique en temps réel avec les nouvelles données enregistrées. Pour ce faire, il suffit de définir un intervalle de temps entre 
chaque génération du graphique et de contrôler à l'intérieur de celui-ci si la date de fin est bien la date actuelle. Si cela est le cas, alors on peut lancer la génération d'un nouveau graphique.

.. captionup:: graph.js
.. code-block:: js
    
    window.setInterval(function() {
        var endDate = new Date(end);
        var now = new Date();
        var today = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 1);
        if (today.getTime() == endDate.getTime()) {
            socket.emit("requestData", startDate.getTime(), endDate.getTime(), values);
        }
    }, 60000);
