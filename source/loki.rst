Conception d'une base de données avec Loki.js
#############################################

Présentation
************

*Loki.js* est une base de données orientée documents écrite en JavaScript. Elle permet ainsi de stocker des objets dans un fichier *JSON*. Son principal avantage est sa simplicité. Pas besoin de *SQL* pour la manipuler, une simple connaissance 
de ses méthodes principales est requise afin d'effectuer des opérations basiques. Il faut cependant noter que celle-ci n'est pas adaptée pour de très grands projets, auquel cas *MongoDB* ou *MySQL* seraient préférés.

Utiliser Loki.js
****************

L'installation est elle aussi très simple. En effet, le *Node Package Manager* s'en occupe pour nous.

.. code-block:: sh

   $ npm install lokijs
   
Il ne reste plus qu'à créer un fichier au format *JSON* dans le répertoire du serveur. En ce qui concerne le code, il faut commencer par importer le module. On peut ensuite en instancier un objet prenant en paramètre le nom 
du fichier *JSON* que l'on souhaite utiliser.

.. code-block:: js

    var LokiDB = require('lokijs');
    var database = new LokiDB("LokiDatabase.json");

Les collections
...............

Étant une base de données orientée documents, *Loki.js* utilise des **collections** pour sauvegarder des valeurs. Vous pouvez vous représenter ces dernières comme de grandes boîtes portant chacune une 
étiquette décrivant ce qu'elles doivent contenir. Des objets peuvent être rangés à l'intérieur de celles-ci, pour autant qu'ils correspondent au contenu de la boîte. Ceux-ci peuvent chacun posséder leurs propres caractéristiques. 
Afin de visualiser ces collections, observez le :ref:`collections`

.. _collections:

.. figure:: figures/collections.png
   :align: center
   :figwidth: 70%
   :scale: 75%
   
   schéma des collections

Vous l'aurez compris, ces fameux objets sont en réalité bel et bien des objets *JSON*. De ce fait, il devient très facile d'organiser les données et de créer un programme bien structuré. Nous allons maintenant voir comment instancier ces collections 
dans le code. Afin d'être sûr de bien comprendre le sujet, je vous invite à faire des tests tout au long des explications.

Pour créer une collection, le prototype est le suivant (pour autant que vous ayez nommé l'objet de la base de données ``database``).

.. code-block:: js
   
   var maCollection = database.addCollection("nomDeMaCollection");
   
Pour ajouter un objet dans la collection, on utilise la fonction ``insert()`` qui prend l'objet désiré en paramètre. Pour reproduire l'exemple de la collection ``mammifere`` du schéma, cela donnerait ceci :

.. code-block:: js

    var mammifere = database.addCollection("mammifere");
   
    mammifere.insert({
        animal: "mouton",
        couleur: "blanc",
        age: 4
    });
    
    mammifere.insert({
        animal: "chat",
        couleur: "roux",
        nom: "Félix"
    });
    
    mammifere.insert({
        animal: "dauphin",
        couleur: "gris",
        age: 6,
        habitat: "mer"
    });
    
.. admonition:: note

   Les objets d'une collection sont référencés par un identifiant partant de ``1`` et s'incrémentant à chaque insertion.
   
Si vous souhaitez retirer un objet d'une collection, vous pouvez utiliser la fonction ``remove()``. Celle-ci prend en paramètre l'objet que l'on souhaite enlever ou son identifiant.

Sauvegarder la base de données
..............................

Pour l'instant, nous avons ajouté une nouvelle collection à la base de données. Cependant, celle-là se trouve encore dans une mémoire tampon et n'est pas enregistrée dans notre fichier *JSON*. Afin d'écrire la collection dans ce fichier, 
il faut utiliser la méthode ``saveDatabase()``. Il faudra également exécuter la méthode ``loadDatabase(options, callback)`` en début de programme afin de charger la base de données.

.. code-block:: js

   database.loadDatabase({}, function(){
   });
   database.saveDatabase();
  
Dès le prochain lancement du programme, la collection ``mammifere`` ne sera donc plus à créer mais à importer avec la fonction ``getCollection("nomDeMaCollection")``. Ce chargement doit impérativement être effectué après que *Loki.js* ait correctement 
démarré ; la collection risque sinon d'être introuvable. Afin d'éviter ce problème, il suffit de l'exécuter à l'intérieur de la fonction de callback de ``loadDatabase``.

.. code-block:: js
   
   var mammifere
   
   database.loadDatabase({}, function(){
       mammifere = database.getCollection("mammifère");
       
       if (measurements === null) {
           mammifere = database.addCollection("mammifere");
       }
   });
   
.. admonition:: note

   La condition sert ici à instancier une collection, si elle n'existe pas déjà.
   
Récupérer les objets d'une collection
.....................................

Nous avons mis en place une base de données fonctionnelle qui permet d'enregistrer toutes sortes d'informations. Il est maintenant temps d'apprendre à les extraire des collections.

Reprenons la collection des mammifères créée auparavant. Le moyen le plus simple de récupérer l'un de ces objets consiste à utiliser la méthode ``get(ID)``.

.. code-block:: js

   mammifere.get(2); //Retourne {animal: "chat", couleur: "roux", nom: "Félix"}
   
Cependant, nous ne serons souvent pas au courant de l'identifiant de l'objet souhaité. C'est pourquoi il est possible d'effectuer des requêtes, permettant de récupérer des objets possédant des attributs spécifiques. Concrètement, cela reviendrait 
par exemple à dire à la collection : "Renvoie-moi tous les mammifères dont la couleur est grise". Pour cela, la méthode ``find(query)`` est utilisée et prend en paramètre un objet contenant l'attribut qui nous intéresse. Pour obtenir 
le même résultat que dans l'exemple, il faudrait écrire :

.. code-block:: js

   mammifere.find( {"couleur" : "gris"} );
   // Retourne {animal: "dauphin", couleur: "gris", age: 6, habitat: "mer"}
   
Il est également possible d'utiliser des opérateurs comme paramètres de la méthode ``find(query)``. Nous pourrons ainsi recevoir, par exemple, tous les objets dont l'attribut ``age`` est plus grand que ``2``.
L'écriture de ce paramètre, dont vous trouverez le prototype ci-dessous, se complexifie un peu.

..  code-block:: json

    {
        attribut: {
            "$opérateur": valeur
        }
    }
    
Voici une liste [#f1]_  non exhaustive des opérateurs intéressants que vous serez probablement amené à utiliser.

=========  ========================  =======================
Opérateur  Nom anglais               Traduction
=========  ========================  =======================
$eq        equal to                  égal à
$ne        not equal to              n'est pas égal à
$gt        greater than              plus grand que
$gte       greater than or equal to  plus grand que ou égal à
$lt        less than                 moins que
$lte       less than or equal to     moins que ou égal à
$contains  contains                  contient
=========  ========================  =======================


Ainsi, l'exemple cité ci-dessus s'écrit de cette façon :

..  code-block:: js

    mammifere.find({
        age: {
            "$gt": 2
        }
    });
    
Lorsque la méthode renvoie plus d'un objet, ceux-ci sont stockés dans une liste par ordre croissant d'identifiant. Ici, nous aurions : ``[objetMouton, objetDauphin]``.

Il est possible d'aller encore plus loin et d'utiliser les opérateurs ``$or`` et ``$and`` afin de spécifier la recherche. La notation du paramètre se complique d'autant plus.

..  code-block:: js

    {
        "opérateur $or ou $and": [{
            attribut1: {
                "opérateur": valeur
            }
        }, {
            attribut2: {
                "opérateur": valeur
            }
        }]
    }
    
Il est évidemment possible d'ajouter autant d'attributs que souhaité. 

Vous avez désormais assez de connaissances pour mettre en place une base données avec *Loki.js*. Avant de vous lancer dans le TP suivant, testez votre compréhension avec un exercice et un quiz.

Exercice
********

1. Créez une base de données comportant une collection identique à ``meubles`` de :ref:`collections`.

2. Retirez l'objet possédant le nom ``chaise``.

3. Ajoutez un objet possédant les caractéristiques suivantes :
   
   * nom : console
   * matière : bois
   * prix : 1100

4. Faites appel à une requête se référant uniquement au prix, qui renverra le ``bureau`` et la ``console``.

Quiz
****

..  quiz:: quiz1
    :question: "La méthode find() renvoit des"
    :answers: ["objets", "listes", "tuples"]
    :correction: [True, True, False]
    :genre: checkbox
    
..  quiz:: quiz1
    :question: "La base de données se sauvegarde automatiquement lors de l'ajout d'un élément"
    :answers: ["Vrai", "Faux"]
    :correction: [False, True]
    :genre: radio

..  quiz:: quiz1
    :question: "La méthode insert() prend en paramètre"
    :answers: ["Un objet", "Une liste", "Un tuple"]
    :correction: [True, False, False]
    :genre: radio
    :btn_correction: quiz1

.. [#f1] liste tirée de la documentation officielle de Loki.js <http://lokijs.org/#/docs#find>