var toggle = document.getElementsByTagName('i')[0];
toggle.style.position = 'relative';
toggle.style.top = '8px';

var trees = document.getElementsByClassName('highlight-tree');

for (var i = 0; i < trees.length; i++) {
	trees[i].innerHTML = trees[i].innerHTML
		.replace(/\ ([a-z0-9_-]+\n)/gi, '<span style="color:blue"> $1</span>')
		.replace(/\ (socket\.io\n)/gi, '<span style="color:blue"> $1</span>');
}