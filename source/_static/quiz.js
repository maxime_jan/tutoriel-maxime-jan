function byId(id) {
	return document.getElementById(id);
}

function byClass(classe) {
	return document.getElementsByClassName(classe);
}

function checkbox(id_div, question, labels, reponses) {
	var html = '<p>'+question+'</p>';
	for (var i = 0; i < labels.length; i++) {
		var id_random = Math.random().toString(36).substring(7);
		if (reponses[i]=='true') { reponse = 'true'; } else { reponse = 'false'; }
		html += '<div class="checkbox '+id_random+'">';
		html += '<input id="'+id_random+'"class="rep styled '+reponse+'" type="checkbox">';
		html += '<label class="'+id_random+'" for="'+id_random+'">'+labels[i]+'</label>';
		html += '</div>';
	}
	byId(id_div).innerHTML += html;
}

function radio(id_div, question, labels, reponses) {
	var html = '<p>'+question+'</p>';
	id_global = Math.random().toString(36).substring(7);
	for (var i = 0; i < labels.length; i++) {
		id_random = Math.random().toString(36).substring(7);
		if (reponses[i]=='true') { reponse = 'true'; } else { reponse = 'false'; }
		html += '<div class="radio '+id_random+'">';
		html += '<input id="'+id_random+'" name="'+id_global+'" class="rep styled '+reponse+'" type="radio">';
		html += '<label class="'+id_random+'" for="'+id_random+'">'+labels[i]+'</label>';
		html += '</div>';
	}
	byId(id_div).innerHTML += html;
}

function corriger(id_div) {
	var a_corriger = byClass(id_div);
	var erreurs = 0;
	for (var h = 0; h < a_corriger.length; h++) {
		var reps = byClass(id_div)[h].getElementsByClassName('rep');
		for (var i = 0; i < reps.length; i++) {
			if (/true/.test(reps[i].className)) {
				if (!byId(reps[i].id).checked) {
					if (/checkbox/.test(byClass(reps[i].id)[0].className)) {
						if (!/checkbox-danger/.test(byClass(reps[i].id)[0].className)) {
							byClass(reps[i].id)[0].className += ' checkbox-danger';
						}
					}
					if (/radio/.test(byClass(reps[i].id)[0].className)) {
						if (!/radio-danger/.test(byClass(reps[i].id)[0].className)) {
							byClass(reps[i].id)[0].className += ' radio-danger';
						}
					}
					byClass(reps[i].id)[1].style.color = 'red';
					erreurs++;
				} else {
					if (/checkbox/.test(byClass(reps[i].id)[0].className)) {
						if (!/checkbox-success/.test(byClass(reps[i].id)[0].className)) {
							byClass(reps[i].id)[0].className += ' checkbox-success';
						}
					}
					if (/radio/.test(byClass(reps[i].id)[0].className)) {
						if (!/radio-success/.test(byClass(reps[i].id)[0].className)) {
							byClass(reps[i].id)[0].className += ' radio-success';
						}
					}
					byClass(reps[i].id)[1].style.color = 'green';
				}
			}
			if (/false/.test(reps[i].className)) {
				if (byId(reps[i].id).checked) {
					if (/checkbox/.test(byClass(reps[i].id)[0].className)) {
						if (!/checkbox-danger/.test(byClass(reps[i].id)[0].className)) {
							byClass(reps[i].id)[0].className += ' checkbox-danger';
						}
					}
					if (/radio/.test(byClass(reps[i].id)[0].className)) {
						if (!/radio-danger/.test(byClass(reps[i].id)[0].className)) {
							byClass(reps[i].id)[0].className += ' radio-danger';
						}
					}
					byClass(reps[i].id)[1].style.color = 'red';
					erreurs++;
				} else {
					if (/checkbox/.test(byClass(reps[i].id)[0].className)) {
						if (!/checkbox-success/.test(byClass(reps[i].id)[0].className)) {
							byClass(reps[i].id)[0].className += ' checkbox-success';
						}
					}
					if (/radio/.test(byClass(reps[i].id)[0].className)) {
						if (!/radio-success/.test(byClass(reps[i].id)[0].className)) {
							byClass(reps[i].id)[0].className += ' radio-success';
						}
					}
					byClass(reps[i].id)[1].style.color = 'green';
				}
			byId(reps[i].id).disabled = true;
			}
		}
		if (erreurs === 0) {
			byId('resultats_'+id_div).innerHTML = '<h4>Bravo ! Vous n\'avez fait aucune erreur</h4>';
		}
		if (erreurs === 1) {
			byId('resultats_'+id_div).innerHTML = '<h4>Vous avez fait '+erreurs+' erreur.</h4>';
		}
		if (erreurs > 1) {
			byId('resultats_'+id_div).innerHTML = '<h4>Vous avez fait '+erreurs+' erreurs.</h4>';
		}
	}
}