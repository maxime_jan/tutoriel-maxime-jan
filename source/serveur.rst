Mise en place du serveur Node.js
################################

Installation de Node.js, Express.js et socket.io
************************************************

Nous allons commencer par travailler sur le serveur *Node* de la station météorologique. Avant tout, il va falloir installer *Node.js*, *Express.js* ainsi que *socket.io*. 


En ce qui concerne *Node.js*, nous utiliserons dans ce tutoriel la version 0.12.6. Le paquet la contenant se trouve dans le template. Il suffit donc d'entrer la commande suivante pour lancer son installation.

.. code-block:: sh

   $ sudo dpkg -i node_0126.deb
   
Pour être sûr que tout s'est déroulé correctement, vous pouvez contrôler la version de Node.js :

.. code-block:: sh

   $ node -v
   
Installer *Express.js* et *socket.io* est maintenant possible grâce au *Node Package Manager*, installé de pair avec *Node.js*, en entrant les commandes :

.. code-block:: sh

   $ npm install express
   $ npm install socketio


Démarrage du serveur
********************

Voyons ensemble le code complet du serveur.

.. captionup:: NodeServer.js
.. code-block:: js
   :linenos:

   var express = require('express');
   var app = express();
   var server = require('http').createServer(app);
   var io = require('socket.io').listen(server);
   var fs = require('fs');

   var port = 8080;

   app.get('/', function (req, res) {
      res.sendFile("/home/pi/tm-maxime-jan/source/client/html/index.html");
   });

   app.use(express.static("/home/pi/tm-maxime-jan/source/client/static"));

   server.listen(port, function() {
      console.log("Le serveur a été démarré sur le port" + port);

   });

   var socketFile = require("./socketio")(io);

Rien ne devrait particulièrement vous étonner. Cependant, voici tout de même quelques explications au cas où tout ne serait pas clair.

*  **1-5** Tous les modules nécessaires au serveur sont chargés.
*  **7**  Une variable ``port`` est créée, contenant le port sur lequel le serveur va démarrer.
*  **9-11**  Comme la station météorologique ne comporte qu'une seule page, celle-ci est envoyée au client lorsqu'il demande la route racine.
*  **13** Un dossier ``static`` est défini à l'intérieur duquel se trouvent tous les fichiers relatifs au client.
*  **15-18** Le serveur écoute le port choisi.
*  **20** Afin d'organiser le code source, la partie concernant *socket.io* sera séparée du reste du serveur. Pour ce faire, on importe la fonction ``io`` du fichier ``socketio.js``.

.. captionup:: socketio.js
.. code-block:: js
   :linenos:
   :emphasize-lines: 1
   
   exports = module.exports = function(io) {
      io.sockets.on("connection", function(socket) {
         console.log("Un client s'est connecté");
        });
   }

En observant le code de base du fichier ``socketio.js``, vous pouvez en effet constater que la fonction ``io`` est exportée. C'est dans celle-ci que vous écrirez toute la logique de *socket.io*.

Maintenant que vous êtes sûr de comprendre le code, vous allez pouvoir démarrer le serveur. Rendez-vous dans le sous-dossier ``server``, là où se trouve le fichier ``NodeServer.js`` et lancez la commande :

.. code-block:: sh
   
   $ node NodeServer.js
   
Vous devriez alors voir apparaître dans la console un message vous confirmant le démarrage du serveur. Vous pouvez dès lors déjà consulter la page Web. Il ne s'agit bien évidemment, pour l'instant, que de la structure *HTML*.
Entrez dans votre navigateur Web l'adresse IP du Raspberry Pi suivi de deux-points et du port choisi. Si le serveur est correctement enclenché, vous arriverez sur la page ``index.html`` !

Pour éteindre le serveur, pressez **CTRL+C**.



