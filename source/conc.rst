Mot de fin
##########

Le tutoriel touche à sa fin. Si vous avez réussi à le suivre et à réaliser les travaux pratiques, félicitations ! Il ne s'agit en aucun cas d'un travail facile et vous pouvez être fier d'y être arrivé. Votre effort est maintenant non seulement 
récompensé d'un très bel objet que vous pouvez utiliser, mais également d'un grand nombre de compétences que vous pourrez appliquer dans vos futurs projets. En effet, vous êtes désormais capable de développer une application Web de A à Z. Vous savez 
gérer une petite base de données, établir des communications entre le client et le serveur, créer un design *responsive* et bien d'autres choses encore. En plus de cela, vous avez appris à vous servir d'un Raspberry Pi. La communauté derrière 
ce nano-ordinateur est gigantesque et il vous suffira d'une recherche Web pour trouver des centaines de projets très intéressants qui sont maintenant à votre portée. Ce cours n'était qu'un début ; vous êtes maintenant vivement encouragé 
à poursuivre dans cette même direction et à continuer de prendre du plaisir avec la programmation.


.. only:: latex

   Remerciements
   #############
   
   Je souhaite exprimer ma sincère gratitude envers Monsieur Cédric Donner et Monsieur Fabian Simillion pour leur aide et leurs précieux conseils - autant techniques que pédagogiques - sans lesquels ce projet n'aurait pas été possible. 
   Je tiens également à remercier Monsieur Marcello Barile, développeur du module ``node-grovepi``, d'avoir été si prompt à m'écouter et à m'aider. 
   Mille mercis à mon collègue, Samuel Fringeli, pour m'avoir épaulé au long de ce travail ; notre entraide fut primordiale. Je remercie finalement Emily Joynes d'avoir eu la patience et la minutie de relire et de corriger  
   mon travail.

   .. raw:: latex
   
      \clearpage

