Prendre le contrôle du Raspberry Pi
###################################

Interfaces graphiques et consoles
.................................


Dans la vie de tous les jours, on a l'habitude d'utiliser des ordinateurs possédant une interface graphique. C'est-à-dire que notre ordinateur est relié à un écran sur lequel on peut, par exemple, faire apparaître une image
ou voir le curseur se déplacer lorsque l'on bouge la souris.

Pour notre projet, nous emploierons cependant l'interface console du Raspberry Pi. En effet, comme ce dernier servira de serveur, nous n'aurons pas besoin de ses fonctionnalités graphiques. Il va donc falloir apprendre à
maîtriser cette interface composée uniquement de texte.

Utiliser la console et s'y repérer peut paraître laborieux au début, mais s'avère très utile avec un peu de pratique. Nous allons donc étudier les notions **essentielles** pour une bonne prise en main.

Commandes
.........

Commençons par étudier ce que vous apercevez dans votre interface console si vous n'avez encore touché à rien :

.. code-block:: sh
    
    pi@raspberrypi ~ $
    
Que veut donc dire cette ligne ?

* ``pi`` indique la session sur laquelle vous vous trouvez actuellement. Rappelez-vous, il s'agit du nom de login que nous avons entré plus tôt.
* ``raspberrypi`` correspond au nom de la machine. 
* Le tilda (~) indique le chemin d'accès du répertoire de la session ``pi``. Pour l'instant, rien ne suit le tilda car nous sommes dans le répertoire racine de la session.
* Le signe ``$`` invite l'utilisateur à entrer une commande.

Ces commandes que l'on peut entrer vont interagir directement avec le système d'exploitation. C'est donc grâce à elles que l'on va pouvoir contrôler notre Raspberry Pi. Etudions donc celles qui nous serons indispensables
au bon déroulement du projet.

Pour créer un dossier, on utilise la commande :

.. code-block:: sh
    
    $ mkdir nom_du_dossier
    
``mkdir`` est l'abréviation de "make directory", littéralement "créer répertoire" en français. Il suffit d'écrire cette commande suivie du nom de dossier souhaité pour le créer.

Afin de connaître le contenu d'un répertoire, on peut exécuter la commande :

.. code-block:: sh
    
    $ ls
    $ ls nom_d'un_sous_dossier
    
``ls`` liste tous les dossiers et fichiers contenus dans le répertoire indiqué. Si aucun répertoire n'est précisé, on obtient le contenu de celui dans lequel on se trouve. 


Pour se déplacer d'un dossier à un autre, on utilise la commande ``cd`` ("change directory" ou "changer de répertoire" en français) :

.. code-block:: sh

    $ cd ..
    $ cd nom_d_un_sous_dossier
    $ cd nom_d_un_sous_dossier/nom_d_un_sous_sous_dossier
    
La commande ``touch`` permet quant à elle de créer un fichier :

.. code-block:: sh

    $ touch nom_du_fichier.extension
    
    
    
Plusieurs éditeurs de texte sont déjà préinstallés sur le système d'exploitation. Je vous conseille d'utiliser *Nano*, qui se démarque par sa simplicité. Pour le lancer, on exécute la commande suivante :

.. code-block:: sh

    $ sudo nano nom_du_fichier.extension

Vous remarquerez que la ligne commence ici par ``sudo``. Cette instruction permet de lancer n'importe quelle commande en mode administrateur. Pour les commandes vues précédemment, il était inutile de le faire, 
comme celles-ci sont accessibles pour tous les utilisateurs. En revanche, *Nano* refuse d'enregistrer toutes modifications apportées au fichier si l'on ne dispose pas des droits d'administrateur.

Nous utiliserons uniquement *Nano* pour de petites modifications de fichiers. En effet, développer une application Web dans une interface console serait une tâche longue et fastidieuse. 
Nous mettrons donc, plus tard, en place une manière d'écrire le code sur votre éditeur de texte favori.  

Afin de supprimer des fichiers ou des dossiers, on utilise la commande :

.. code-block:: sh

    $ sudo rm nom_du_fichier.extension
    $ sudo rm -r nom_du_dossier
    
``rm`` (abréviation de "remove", en français "enlever") suivi par le nom d'un fichier permet de détruire ce dernier. Si l'on désire supprimer un dossier, il faut ajouter l'option ``-r`` précisant que l'action va non seulement supprimer le répertoire, mais également tous ses sous-répertoires.  



.. TIP::

    Écrire à chaque fois les noms de dossiers et de fichiers dans une commande peut s'avérer être une tâche longue et répétitive.
    
    Pour aller plus vite, écrivez simplement le début du nom que vous souhaitez entrer et pressez la touche **TAB**. Le système d'exploitation va ainsi tenter de compléter par lui-même le reste du nom.
    

Finalement, pour redémarrer le Raspberry, il faut effectuer la commande :

.. code-block:: sh

    $ sudo reboot

Et pour l'éteindre :

.. code-block:: sh

    $ sudo halt

Avant de clore cette section, il faut encore savoir que *PuTTY* propose également d'utiliser les boutons de la souris. Ceux-ci vont servir à effectuer les actions de copier/coller.

* Bouton gauche : Permet de séléctionner du texte. Une fois la séléction effectuée, celle-ci est automatiquement copiée.
* Bouton droit : Permet de coller.

.. only:: latex

   .. raw:: latex
   
      \clearpage
      

   
   Bibliographie
   #############

   * NEBRA, Mathieu. *Réalisez votre site web avec HTML5 et CSS3*. Paris : OpenClassrooms - ex-Site du Zéro, 2011, 320 p.
   * SARRION, Eric. *Programmation avec Node.js, Express.js et MongoDB*. Paris : Eyrolles, 2014, 586 p.

   Webographie
   ##########
   
   * ASGALLANT. "Make Google Charts Gauge responsive". Consulté le 12 décembre 2015. <http://stackoverflow.com/questions/18979535/make-google-chart-gauge-responsive>
   * BARILE, Marcello. "node-grovepi". Consulté le 15 août 2015. <https://www.npmjs.com/package/node-grovepi>.
   * BENGHOZI, Pierre-Jean ; BUREAU, Sylvain ; MASSIT-FOLLEA, Françoise. "Définir l’internet des objets". Consulté le 7 janvier 2016. <http://books.openedition.org/editionsmsh/84?lang=fr>.
   * BEULQUE, Pieter. "Raspberry Pi + NodeJS". Consulté le 2 août 2015. <http://weworkweplay.com/play/raspberry-pi-nodejs/>.
   * BOOTSNIPP. "Button Builder for Bootstrap 3". Consulté le 11 octobre 2015. <http://bootsnipp.com/buttons>.
   * CHAVELLI, Maurice. "Prenez en main Bootstrap". Consulté le 10 octobre 2015. <https://openclassrooms.com/courses/prenez-en-main-bootstrap>.
   * CODACADEMY. "JQuery". Consulté le 24 août 2015. <https://www.codecademy.com/fr/learn/jquery>.
   * DALMARIS, Peter. "Raspberry Pi: Full Stack". Consulté le 27 juillet 2015. <https://www.udemy.com/rpi-full-stack/#/lecture/3109460>.
   * ELV. Consulté le 17 mars 2016. <http://www.elv.ch/output/controller.aspx?refid=SEM_30010>.
   * ETERNICODE. "bootstrap-datepicker sandbox". Consulté le 11 octobre 2015. <http://eternicode.github.io/bootstrap-datepicker/>.
   * FILEZILLA. "The free FTP solution". Consulté le 30 août 2015. <https://filezilla-project.org/>.
   * GOOGLE DEVELOPERS. "Using Google Charts". Consulté le 19 octobre 2015. <https://developers.google.com/chart/interactive/docs/>.
   * HAVERBEKE, Marijn. "JavaScript Eloquent". Consulté le 17 mars 2016. <http://fr.eloquentjavascript.net/>
   * LETIAGOLVES. "How to limit the number of selected checkboxes". Consulté le 18 mars 2016. <http://stackoverflow.com/questions/19001844/how-to-limit-the-number-of-selected-checkboxes>
   * LOKIJS. Consulté le 17 mars 2016. <http://lokijs.org/#/>.
   * NEBRA, Mathieu. "Apprenez à créer votre site web avec HTML5 et CSS3". Consulté le 17 mars 2016. <https://openclassrooms.com/courses/apprenez-a-creer-votre-site-web-avec-html5-et-css3>.
   * NEBRA, Mathieu. "Des applications ultra-rapides avec Node.js". Consulté le 17 mars 2016. <https://openclassrooms.com/courses/des-applications-ultra-rapides-avec-node-js>.
   * PI-SHOP. Consulté le 17 mars 2016. <https://www.pi-shop.ch/>
   * PITTMAN, Cameron ; WILLIAMS, James. "JavaScript Basics". Consulté le 21 juillet 2015. <https://www.udacity.com/course/javascript-basics-->.
   * SK PANG ELECTRONICS. "I2C Installation for Raspberry Pi – Step by Step Guide". Consulté le 2 août 2015. <http://skpang.co.uk/blog/archives/575>.
   * THE INTERNET OF THINGS COUNCIL. "The Internet of Things". Consulté le 7 janvier 2016. <http://www.theinternetofthings.eu/>.
   * TIOBE. "Tiobe Index for January 2016". Consulté le 7 janvier 2016. <http://www.tiobe.com/tiobe_index>.
   * TOPPREISE. Consulté le 17 mars 2016. <https://fr.toppreise.ch/index.php>
   * VRSALOVIC VINKO. "Best way to find if an item is in a JavaScript array". Consulté le 15 mars 2016. <http://stackoverflow.com/questions/143847/best-way-to-find-if-an-item-is-in-a-javascript-array>.

