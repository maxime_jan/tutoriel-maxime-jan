Installation de Raspbian sur une carte microSD
##############################################

Avant de pouvoir démarrer le Raspberry Pi, nous allons devoir installer un système d'exploitation sur la carte microSD. Dans ce tutoriel, nous utiliserons Raspbian, un système d'exploitation dérivé de Debian et optimisé pour le 
Raspberry Pi. Le processus d'installation est détaillé dans la vidéo suivante.

..  only:: html

    .. raw:: html
    
       <center><iframe width="560" height="315" src="https://www.youtube.com/embed/kvwO5jeN5SY" frameborder="0" allowfullscreen></iframe></center>
    
.. only:: latex

   .. image:: figures/QRSD.png
      :scale: 20%
      :align: center

   ..  raw:: latex
   
       ~
       \begin{center}
       QR code de la vidéo : https://www.youtube.com/watch?v=kvwO5jeN5SY
       \end{center}
