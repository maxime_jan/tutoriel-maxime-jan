TP : Créer un design responsive
###############################

Comme cela avait été évoqué lors de l'introduction, Bootstrap va nous permettre de créer un design *reponsive* afin que l'application Web soit lisible à la fois sur ordinateurs, mais égalements sur tablettes et téléphones portables.
Cependant, Boostrap n'est pas capable de redimensionner le graphique. Pour ce faire, ajoutez les lignes suivantes [#f1]_ à la fin de la fonction ``drawChart(dataList)``.

.. captionup:: graph.js
.. code-block:: js
    
    function drawChart(dataList) {
        //...
        function resizeHandler() {
            chart.draw(dataTable, options);
        }
        if (window.addEventListener) {
            window.addEventListener('resize', resizeHandler, false);
        } else if (window.attachEvent) {
            window.attachEvent('onresize', resizeHandler);
        }
    });
    
En ce qui concerne les éléments de l'application, vous êtes libre de les disposer comme bon vous semble ; aucune correction ne sera donc proposée pour ce TP. En revanche, si vous débutez 
avec Bootstrap et son système de grille, il vous est fortement conseillé de suivre les modèles :ref:`bst_wide` et :ref:`bst_sm`. Bon travail !

.. _bst_wide:

.. figure:: figures/tp_bootstrap_wide.png
   :align: center

   grille écran large
   
.. _bst_sm:

.. figure:: figures/tp_bootstrap_sm.png
   :align: center
   :scale: 50%

   grille très petit écran
   
   
.. [#f1] Solution trouvée sur <http://stackoverflow.com/questions/18979535/make-google-chart-gauge-responsive>.