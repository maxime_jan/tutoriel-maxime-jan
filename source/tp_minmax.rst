TP : Afficher les valeurs maximales et minimales
################################################

Direction à prendre
*******************

Afin de peaufiner la station météorologique, vous allez, dans ce TP, afficher les valeurs minimales et maximales du graphique. Pour vous aider, vous possédez le fichier ``maxMin.js`` dans lequel se trouvent les fonctions 
``FindLowest(dataList, size)`` et ``FindHighest(dataList, size)`` qui renvoient une liste de longueur ``size`` comportant le plus petit et le plus grand des éléments de ``dataListe``. 
Le premier paramètre de ces fonctions correspond évidemment à la liste de données et le second à la longueur de la liste ``values``. Les étapes à réaliser sont les suivantes :

* Dans le fichier ``index.html``, insérer des balises ``div`` pour chacune des données maximales et minimales à afficher.
* Lors du renvoi des données au client, rechercher la plus petite et la plus grande de chaque type avec les fonctions qui vous ont été décrites.
* Modifier le texte des balises ``div`` en fonction des données minimales et maximales ainsi que de leur nature (température, humidité ou pression).
* Si un seul type de valeur est affiché dans le graphique, cacher les autres valeurs minimales et maximales avec la fonction ``hide()`` de *JQuery*, puis les réafficher avec ``show()`` lorsque deux valeurs sont à nouveau 
  affichées.

Ce travail pratique n'est pas très long ni très compliqué. Cependant, je vous encourage à essayer d'éviter le plus de redondances possibles. Bon travail !

Corrections
***********

Passons à la correction. Commençons par insérer les balises ``div`` dans le fichier *HTML*. Quatre balises sont créées pour les valeurs minimales et maximales et quatre autres pour leur label. Cette distinction est faite afin de pouvoir 
facilement gérer l'apparence de l'application Web par la suite.
En les numérotant, il sera ensuite possible de les parcourir à l'aide d'une boucle ``for``.

.. captionup:: index.html
.. code-block:: html
    
    <div id="max1Label"></div>
    <div id="max1"></div>
    
    <div id="min1Label"></div>
    <div id="min1"></div>

    <div id="max2Label"></div>
    <div id="max2"></div>
    
    <div id="min2Label"></p>
    <div id="min2"></div>

Voyons maintenant le code JavaScript. Nous allons écrire ce dernier dans l'événement *socket* ``returnData`` afin que les valeurs soient mises à jour à chaque clique sur le bouton. La première étape est de rechercher les 
données minimales et maximales.

.. captionup:: graph.js
.. code-block:: js
    
        var min = FindLowest(data, values.length);
        var max = FindHighest(data, values.length);
        
Mettons en place une boucle ``for`` qui s'occupera de parcourir toutes les balises ``div``. Comme les identifiants de celles-ci commencent par ``1``, cela doit également être l'index de départ de la boucle. Le nombre d'itérations devra 
être égal au nombre de types de valeurs du graphique. Ce nombre doit être incrémenté de ``1``, étant donné que l'index commence à ``1`` et non ``0``.

.. captionup:: graph.js
.. code-block:: js
    
        for (var i = 1; i < (values.length + 1); i++) {
        }
        
Afin de pouvoir plus tard modifier les balises ``div``, il faut trouver quelle est l'unité du type de valeurs actuel. Une structure ``switch`` permet de rapidement passer en revue les trois cas. Attention à ne pas 
oublier d'enlever ``1`` à l'index.

.. only:: latex

   ..  raw:: latex
   
       ~
       \begin{center}
       ~
       \end{center}
       ~
       \begin{center}
       ~
       \end{center}

.. captionup:: graph.js
.. code-block:: js
    
    var unit;
    for (var i = 1; i < (values.length + 1); i++) {

        switch(values[i-1]){

            case "Température":
            unit = "°C";
            break;

            case "Humidité":
            unit = "%";
            break;   

            case "Pression":
            unit = "hPa";
            break;
        }
    }
    
On peut maintenant modifier les balises des valeurs minimales et maximales ainsi que leur label. En transformant l'index en ``string`` grâce à la fonction ``toString()``, il est ensuite possible de former le nom complet de 
chaque balise.


.. captionup:: graph.js
.. code-block:: js

    $("#min" + i.toString() + "Label").text(values[i - 1] + " minimale");
    $("#min" + i.toString()).text(min[i - 1].toString() + unit);
    
    $("#max" + i.toString() + "Label").text(values[i - 1] + " maximale");
    $("#max" + i.toString()).text(max[i - 1].toString() + unit);

Afin de mieux comprendre cet extrait de code, voici à quoi correspond la première ligne lors de la première et de la deuxième itération :

* Itération 1 : ``$("#min1Label").text(Température minimale");``
* Itération 2 : ``$("#min2Label").text(Pression minimale");``

Il ne manque maintenant plus qu'à cacher les balises portant l'identifiant ``2`` lorsqu'une seule itération a lieu.

.. captionup:: graph.js
.. code-block:: js
    
    if (values.length == 1) {
        $("#min2Label").hide();
        $("#min2").hide();
        $("#max2Label").hide();
        $("#max2").hide();
    }
    
Avec ceci, le TP est terminé. Je vous invite à poursuivre le tutoriel afin de rendre l'application plus agréable d'utilisation.