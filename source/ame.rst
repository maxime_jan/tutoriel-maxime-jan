Améliorations possibles
#######################

A ce stade du tutoriel, vous avez entièrement reproduit l'application Web qui vous a été présentée lors de l'introduction ! Cependant, celle-ci n'est pas parfaite. Beaucoup de fonctionnalités peuvent encore être ajoutées. Ce 
chapitre vous présente différentes idées que vous pourrez essayer d'implanter dans votre projet.

Réaliser des prévisions météorologiques
***************************************

Il serait extrêmement audacieux de prétendre pouvoir faire des prévisions précises avec notre petite station. La météorologie est en effet un domaine très complexe. Afin de réaliser de vraies prédictions, il faudrait non 
seulement posséder plusieurs stations météorologiques partout dans le monde, mais également traiter leurs données avec de puissants algorithmes sur des superordinateurs. Il est donc évident qu'avec des connaissances basiques en météorologie 
et avec la puissance de calcul d'un Raspberry Pi, il est impossible de produire des prédictions fiables.

Cependant, il est tout de même possible, grâce à la pression atmosphérique, de définir s'il va faire beau ou mauvais temps. Par basses pressions (moins de 1013hPa), le ciel aura tendance à être couvert et par hautes pressions (plus de 1013hPa)
à être découvert. Ainsi, en analysant l'évolution de la pression durant les dernières heures, on peut déterminer le temps qu'il va faire plus tard.

Inclure de nouveaux capteurs
****************************

Vous aurez peut-être envie de ne pas vous restreindre au baromètre et au capteur de température et d'humidité. La gamme de capteurs GrovePi est en effet très large et beaucoup pourraient trouver leur place dans notre 
projet (capteur de luminosité, de qualité de l'air, de CO2, etc...). Si celui que vous choisissez est déjà présent dans la `liste`_ des capteurs pris en charge par ``node-grovepi``, alors son implémentation suivra exactement le 
même schéma que ceux que nous avons installés dans ce cours. Si en revanche ils ne font pas partie de la liste, vous devrez écrire votre propre fonction, comme cela est expliqué sur la `page officielle`_ du module.

Créer un système de gestion des données
***************************************

Il est tout à fait imaginable que vous laissiez fonctionner votre station météorologique pendant plusieurs mois ou même plusieurs années. Il arrivera alors un moment où la mémoire saturera car trop de données auront été 
enregistrées. De plus, il n'est pas nécessaire de posséder les valeurs de chaque minute pour une date qui remonte à trois mois en arrière. Une valeur toutes les trente minutes ou toutes les heures serait amplement 
suffisante. 

Pour ce faire, on peut développer un système de gestion de données. S'exécutant par exemple une fois par jour, celui-ci permettrait de supprimer les données superflues.


.. _liste: https://github.com/marcellobarile/GrovePi/tree/master/Software/NodeJS/libs/sensors

.. _page officielle: https://www.npmjs.com/package/node-grovepi
