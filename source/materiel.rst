Matériel nécessaire
###################

Si vous êtes maintenant décidé à vous lancer dans ce tutoriel, vous aurez besoin de vous procurer le matériel nécessaire à la réalisation de la station météorologique. De nombreux revendeurs proposent les articles 
de la liste ci-dessous, comme par exemple `TopPreise`_ ou `ELV`_. Vous trouverez ici les liens d'achat du site `Pi-Shop`_. Celui-ci n'est pas le meilleur marché, mais a l'avantage de proposer tous les articles dont vous 
devrez faire l'acquisition. 

==================================== ========================================================================== =======
Article                              Lien Pi-Shop                                                               Prix
==================================== ========================================================================== =======
Raspberry Pi Model B                 https://www.pi-shop.ch/raspberry-pi-2-model-b                              43.90fr
GrovePi+                             https://www.pi-shop.ch/erweiterungsboard-grovepi                           34.90fr
Capteur de température et d'humidité https://www.pi-shop.ch/grove-temperature-humidity-sensor-pro               19.90fr
Capteur de pression                  https://www.pi-shop.ch/grove-barometer-high-accuracy                       25.90fr
Alimentation                         https://www.pi-shop.ch/steckernetzteil-microusb-5v-2a                      16.90fr
Wi-Fi dongle                         https://www.pi-shop.ch/miniature-wifi-802-11b-g-n-module-fuer-raspberry-pi 16.90fr
Carte microSD 8GB + adaptateur SD    https://www.pi-shop.ch/kingston-microsdhc-card-8gb-class-10-multi-kit      11.90fr
==================================== ========================================================================== =======

Une fois en possession de tous les objets de cette liste, vous êtes prêt à commencer le tutoriel.

.. _TopPreise: https://fr.toppreise.ch/

.. _ELV: http://www.elv.ch/output/controller.aspx?refid=SEM_30010

.. _Pi-Shop: https://www.pi-shop.ch/