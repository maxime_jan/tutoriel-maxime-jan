TP : Enregistrer les données des capteurs
#########################################

Direction à suivre
******************

Maintenant que vous êtes capable d'utiliser *Loki.js*, vous allez pouvoir mettre ces connaissances en pratique. Le but de ce TP est de créer une base de données dans laquelle vous enregistrerez les données émises par 
les capteurs. Plusieurs objectifs sont à atteindre :

* Enregistrement des trois types de données dans un seul objet.
* Dans le même objet, enregistrement de la date exacte (en millisecondes) à laquelle ont été prises les données.
* Définition d'un délai entre chaque sauvegarde de données afin de ne pas surcharger la carte microSD du Raspberry Pi.

Afin de vous aider dans leur réalisation, ouvrez le fichier ``dataHandler.js`` où vous allez écrire le code *Loki.js*. Vous y trouverez le code minimal permettant d'instancier une base de données ainsi que des 
fonctions vides. Votre devoir sera de compléter la fonction ``saveData(currentDHTValue, currentBarometerValue)``. Avec cette aide, ce TP ne devrait vous poser aucun problème.

Bon travail !

Corrections
***********

Il est maintenant temps de passer à la correction. La première étape afin de récupérer les valeurs des capteurs dans le fichier ``dataHandler.js`` est d'exporter la fonction ``saveData``. Celle-ci peut ensuite être 
lancée dans le fichier ``socketio.js`` où les variables ``currentDHTValue`` et ``currentBarometerValue`` sont constamment mises à jour.

.. captionup:: socketio.js
.. code-block:: js

    var dataHandler = require("./dataHandler");
    dataHandler.SaveData(currentDHTValue, currentBarometerValue);
    
Afin de créer un délai entre chaque exécution de la fonction ``saveData``, la fonction ``setInterval(callback, millisecondes)`` est employée. Son utilisation est très simple. Il suffit de passer une variable de temps ``t`` 
en millisecondes comme deuxième argument et la fonction de callback s'exécutera une fois toutes les ``t`` millisecondes. Ainsi, pour enregistrer les données une fois par minute on écrit :

.. captionup:: socketio.js
.. code-block:: js

    setInterval(function() {
            dataHandler.SaveData(currentDHTValue, currentBarometerValue);
        }
    }, 60000);
    
Passons maintenant à l'écriture de cette fonction. Nous possédons déjà les trois valeurs des capteurs que nous souhaitons enregistrer : la température, l'humidité relative et la pression atmosphérique. Il ne manque 
plus qu'à instancier un objet ``Date`` afin de récupérer le moment précis de l'enregistrement des données.

.. captionup:: dataHandler.js
.. code-block:: js
    
    function SaveData(currentDHTValue, currentBarometerValue) {
    
    var currentDate = new Date();
    
Vous pouvez dès à présent insérer un nouvel objet dans la collection ``measurements`` comportant ces quatre données. Afin d'être certain de ne perdre aucun objet qui resterait en mémoire tampon, la méthode ``saveDatabase`` 
est appelée après chaque insertion.


.. captionup:: dataHandler.js
.. code-block:: js
    
    measurements.insert({
        temperature: currentDHTValue[0],
        humidity: currentDHTValue[1],
        pressure: currentBarometerValue,
        date: today.getTime()
    });
    
    database.saveDatabase();

La base de données est maintenant fonctionelle. Nous allons dès lors passer au développement côté client. Nous ne reviendrons qu'une seule fois toucher au serveur pour un TP. Je vous invite par ailleurs 
à enregistrer une série d'objet ``measurements`` à intervalle d'une minute. Ceux-ci seront utiles à sa réalisation.