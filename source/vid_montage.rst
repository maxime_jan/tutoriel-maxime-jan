Montage du matériel
###################

Raspbian étant maintenant installé sur la carte microSD, nous pouvons connecter tous les éléments et démarrer le Raspberry Pi pour la première fois. Suivez simplement les étapes de la vidéo ci-dessous.

..  only:: html

    .. raw:: html
    
       <center><iframe width="560" height="315" src="https://www.youtube.com/embed/AaqvjS5Af7o" frameborder="0" allowfullscreen></iframe></center>
    
.. only:: latex


   .. image:: figures/QRMontage.png
      :scale: 20%
      :align: center
   
   ..  raw:: latex
   
       ~
       \begin{center}
       QR code de la vidéo : https://www.youtube.com/watch?v=kvwO5jeN5SY
       \end{center}
