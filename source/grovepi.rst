Récupération des données des capteurs
#####################################

Activation des outils i2c
*************************

Maintenant que notre serveur est prêt, nous allons voir comment utiliser la carte GrovePi afin de faire fonctionner les capteurs.

La première étape importante va être d'activer les outils *I2C*. Souvenez-vous, nous avons déjà commencé ce processus lors du chapitre de configuration. Il va tout d'abord falloir installer ces outils sur le Raspberry Pi :

.. code-block:: sh
   
   $ sudo apt-get install i2c-tools
   
Une fois l'installation terminée, nous allons éditer un fichier afin d'indiquer que ces outils vont devoir être lancés lors du démarrage de la machine. Ouvrez le fichier ``/etc/modules``. Il suffit d'ajouter ``i2c-dev`` sur la dernière ligne, comme ceci :

.. code-block:: sh
   :linenos:
   :emphasize-lines: 8
   
   # /etc/modules: kernel modules to load at boot time.
   #
   # This file contains the names of kernel modules that should be loaded
   # at boot time, one per line. Lines beginning with "#" are ignored.
   # Parameters can be specified after the module name.
 
   snd-bcm2835
   i2c-dev
   
   
Afin de compléter l'activation, effectuez un redémarrage.

Vous pouvez maintenant utiliser la commande ``i2cdetect`` afin de voir quels ports *I2C* sont utilisés.

.. code-block:: sh
   
   $ sudo i2cdetect -y 1
   
Si vous avez correctement branché le GrovePi ainsi que les capteurs, et que les outils *I2C* ont été activés, vous devriez voir apparaître ceci :

.. code-block:: sh

       0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
   00:          -- 04 -- -- -- -- -- -- -- -- -- -- --
   10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
   20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
   30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
   40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
   50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
   60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
   70: -- -- -- -- -- -- 76 --
   
Le port ``04`` correspond au GrovePi et le port ``76`` au baromètre. Le capteur de température et d'humidité est quant à lui digital, et n'apparaît donc pas dans cette liste.

Le module node-grovepi
**********************

Pour contrôler le GrovePi avec *Node.js*, nous aurons besoin du module ``node-grovepi`` [#f1]_ . Installez-le avec la commande suivante (le processus peut prendre une dizaine de minutes à se compléter) :

.. code-block:: sh

   $ npm install node-grovepi-tm

Tout est maintenant en place pour prendre les premières mesures. Cependant, il est important que vous compreniez bien le fonctionnement de ce module pour la suite du tutoriel. Soyez attentif : ce chapitre est suivi d'un TP qui requiert ces 
connaissances. Nous allons donc décortiquer le code source du fichier template ``sensor.js`` qui permet de capturer des données de la manière la plus simple possible. 

.. captionup:: sensor.js
.. code-block:: js
   :linenos:
   
   var GrovePi = require("node-grovepi-tm").GrovePi;
   
La première chose à faire est évidemment de charger le module dans notre application.

..  raw:: latex
   
    ~
    \begin{center}
    ~
    \end{center}
    ~
    \begin{center}
    ~
    \end{center}
    
.. captionup:: sensor.js
.. code-block:: js
   :lineno-start: 3
   
   var Board = GrovePi.board;
   var board;
   
Nous avons ensuite besoin de l'objet prototype ``Board`` qui permet de définir un objet correspondant à la carte GrovePi. Nous créons déjà une variable pour stocker l'objet plus tard.

.. captionup:: sensor.js
.. code-block:: js
   :lineno-start: 6
   
   var DHTDigitalSensor = GrovePi.sensors.DHTDigital;
   var BarometerI2CSensor = GrovePi.sensors.BarometerI2C;
   
   var dhtSensor;
   var barometerSensor;

Nous aurons également besoin des objets prototypes correspondant aux deux capteurs branchés et de deux variables qui contiendront leurs objets.

.. captionup:: sensor.js
..  code-block:: js
    :lineno-start: 11
    
   
    var start = function() {

        board = new Board({
    
            debug: true,
    
            onError: function(err) {
                console.log(err);
            },
    
            onInit: function(res) {
                if (res) {
                    dhtSensor = new DHTDigitalSensor(4, DHTDigitalSensor.VERSION.DHT22, 
                    DHTDigitalSensor.CELSIUS);
                    barometerSensor = new BarometerI2CSensor(BarometerI2CSensor);
    
                    // Temps en millisecondes
                    dhtSensor.watch(100);
                    barometerSensor.watch(100);
                    
                    dhtSensor.on("change", function(res) {
                        console.log("Température =" + res[0]+ " humidité =" + res[1]);
                    })
                    
                    barometerSensor.on("change", function(res) {
                        console.log("Pression =" + res);
                    })
    
                } else {
                    console.log("erreur");
                }
            }
        })
        board.init()
    }
    
La fonction ``start`` est très importante. Comme son nom l'indique, elle va permettre de démarrer le GrovePi. Il faut pour cela créer l'objet ``board`` puis le démarrer avec sa fonction ``init()``. Voyons ensemble les paramètres demandés pour l'instancier. 

* **ligne 11 :** ``debug`` Vous n'aurez pas besoin de changer ce paramètre, laissez-le simplement ``true``.
* **ligne 17 :** ``onError`` Si une erreur se produit, cette fonction de callback se charge de l'afficher. Vous n'avez pas besoin d'y toucher.
* **ligne 21 :** ``onInit`` Trois étapes doivent être effectuées dans cette fonction. 

  1. **Ligne 23-25 :** Il faut tout d'abord créer et paramétrer les objets de chaque capteur. Le constructeur du capteur d'humidité et de température requiert trois arguments que vous devez connaître : ``DHTDigitalSensor(pin, version, type)``.
 
     * **pin** correspond au port digital dans lequel est branché le capteur. Ce chiffre est celui qui suit la lettre **D** à côté du port. Dans cet exemple, le port **D4**
       est utilisé.
     * **version** dépend de la sorte du capteur que l'on utilise. Ce paramètre est déjà adapté à notre capteur.
     * **type** permet de choisir entre ``CELSIUS`` ou ``FAHRENHEIT``.
     
  2. **Ligne 28-29 :** Pour chacun des capteurs, nous devons préciser un délai entre chaque prise de données avec la fonction ``watch``. Attention à bien indiquer le temps en millisecondes.
  3. Chaque capteur déclenche un événement lorsque l'une de ses données change. La fonction de callback comporte le paramètre ``res`` qui contient les valeurs mesurées. Vous pouvez ensuite, à l'intérieur de celle-ci, en faire ce
     que vous voulez. Elles sont pour l'instant sont simplement affichées dans la console.

.. captionup:: sensor.js  
..  code-block:: js
    :lineno-start: 47
    
    function onExit(err) {
        console.log('ending');
        board.close();
        process.removeAllListeners();
        process.exit();
        if (typeof err != 'undefined')
            console.log(err);
    }

    //Détecte l'arrêt du programme
    process.on('SIGINT', onExit);
    
La ligne 57 permet de recevoir l'événement d'arrêt du programme et lance la fonction de callback ``onExit``. Vous n'avez pas besoin de vous en soucier, celle-ci s'occupe d'arrêter le processus du GrovePi correctement.

.. captionup:: sensor.js
..  code-block:: js
    :lineno-start: 59
    
    start();

On lance finalement la fonction créée auparavant afin de démarrer le GrovePi.

Capter les premières données
******************************

Après toute cette théorie, voilà un peu de concret. Nous allons pouvoir faire fonctionner les capteurs. Le code étant en place, il ne reste plus qu'à l'exécuter. Retournez dans le terminal et, comme pour le serveur, utilisez la commande ``node`` pour lancer le programme.

.. code-block:: sh

   $ node sensor.js
   
Vous pouvez voir les données apparaître sur votre écran, et s'actualiser à chaque changement. Pour fermer le programme, pressez **CTRL+C** de la même manière que pour le serveur.

Vous avez à présent toutes les connaissances requises pour vous lancer dans la réalisation du premier TP.

Quiz
****

..  quiz:: quiz1
    :question: "Le premier paramètre de DHTDigitalSensor correspond à l'unité de la température."
    :answers: ["Vrai", "Faux"]
    :correction: [False, True]
    :genre: radio
    
..  quiz:: quiz1
    :question: "L'événement change permet de"
    :answers: ["Changer l'unité de la température", "Recevoir les données lorsqu'elles changent", "Changer le délai de prise des données"]
    :correction: [False, True, False]
    :genre: radio

..  quiz:: quiz1
    :question: "board.init() doit s'éxécuter"
    :answers: ["Au début de la fonction start", "A la fin de la fonction start"]
    :correction: [False, True]
    :genre: radio
    :btn_correction: quiz1


.. [#f1] Nous utilisons ici une version du module modifiée pour utiliser le baromètre. Dépôt officiel : https://www.npmjs.com/package/node-grovepi