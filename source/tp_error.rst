TP : Afficher un message d'erreur
#################################

Direction à prendre
******************

La station météorologique est à présent presque terminée. Dans ces derniers chapitres, nous allons encore implanter quelques fonctionnalités pour rendre son utilisation plus agréable. Comme l'application Web comporte 
deux entrées utilisateur avec lesquels ce dernier peut faire des choix, il serait utile de lui indiquer si ces choix ne sont pas corrects. En l'occurrence, nous allons contrôler s'il a choisi un intervalle de temps dans lequel 
aucune donnée n'a été enregistrée et s'il a au moins coché une checkbox. Notez que l'on pourrait également vérifier que rien d'autre qu'une date ne soit inscrite dans le *datepicker*. Voici quelques étapes pour vous diriger dans 
l'implémentation de ce message d'erreur.

* Introduire une balise ``div`` dans le code HTML afin d'y insérer le message d'erreur.
* Contrôler le nombre de checkbox cochées et la taille de la liste ``data`` que le serveur envoie au client.
* Dans le cas où l'une de ces valeurs est égale à ``0``, changer le texte d'erreur en conséquence, afficher la balise d'erreur et cacher toutes les balises relatives au graphique.
* Lorsqu'il n'y a plus d'erreur, faire réapparaître le graphique et cacher le message d'erreur.

Bon travail !

Corrections
***********

Place aux corrections. Commençons par insérer la balise d'erreur en dessous de celle du graphique. Pour lui donner un meilleur aspect, la classe ``alert alert-danger`` de Bootstrap est ici utilisée.

.. captionup:: index.html
.. code-block:: html
   
   <div class="alert alert-danger" id ="error"></div>

A cause de cette classe Bootstrap, il est indispensable de cacher la balise lors de l'ouverture de la page. Sinon, l'utilisateur apercevrait un cadre rouge en dessous du bouton.

.. captionup:: graph.js
.. code-block:: js

    $(function() {
       $("#error").hide();
    }
   
Il faut ensuite traiter les deux cas d'erreurs possibles. En premier lieu, contrôlons les checkbox lorsque l'utilisateur clique sur le bouton. Si au moins une est cochée, alors l'événement *socket* de génération peut être 
activé. Dans le cas contraire, on lance une fonction ``ErrorSelect()``

.. captionup:: graph.js
.. code-block:: js
    
    if (values.length != 0) {
        socket.emit("requestData", startDate.getTime(), endDate.getTime(), values);
    } else {

        ErrorSelect();
    }
    
Définissons maintenant cette fonction. Comme indiqué dans la consigne, lorsqu'une erreur se produit, il faut commencer par cacher toutes les balises relatives au graphique. On crée pour cela une fonction, car la même opération 
devra être effectuée pour le deuxième type d'erreur. Le texte de la balise d'erreur peut ensuite être modifié et affiché.

.. only:: latex

   ..  raw:: latex
   
       ~
       \begin{center}
       ~
       \end{center}
       ~
       \begin{center}
       ~
       \end{center}

.. captionup:: graph.js
.. code-block:: js
    
    function HideMaxMinChart() {
        $("#min1Label").hide();
        $("#min2Label").hide();
        $("#min1").hide();
        $("#min2").hide();
    
        $("#max1Label").hide();
        $("#max2Label").hide();
        $("#max1").hide();
        $("#max2").hide();
        
        $("#chart").hide();
    }

    function ErrorSelect() {
        HideMaxMinChart()
        $("#error").text("Veuillez séléctionner au moins un type de donnée à afficher dans 
        le graphique.");
        $("#error").show();
    }
    
    
    
Pour contrôler qu'au moins une donnée soit enregistrée dans l'intervalle de temps choisi, on vérifie la longueur de la liste ``data`` dans l'événement *socket* ``returnData``. Si celle-ci est plus grande que ``0``, alors 
le code produit jusqu'ici peut s'effectuer normalement. La balise d'erreur peut alors être cachée et les balises du graphique affichées. Dans l'autre cas, on lance la fonction ``ErrorData()`` qui suit le même principe que ``ErrorSelect()``.

.. captionup:: graph.js
.. code-block:: js
    
    socket.on("returnData", function(data) { 
        if (data.length != 0) {
            for (var i = 1; i < (values.length + 1); i++) {
                //...
                $("#min" + i.toString() + "Label").show();
                $("#min" + i.toString()).show();
                //...
                $("#max" + i.toString() + "Label").show();
                $("#max" + i.toString()).show();
            }
            //...
            $("#chart").show();
            $("#error").hide();
            //...
        }
        else {
            ErrorData();
        }
    });
    
    function ErrorData() {
        HideMaxMinChart()
        $("#error").text("Il n'y a pas de données enregistrées pour l'intervalle 
        séléctionné.");
        $("#error").show();
    }
    
Les messages d'erreur sont ainsi en place ! Je vous encourage à essayer d'aller plus loin et, comme énoncé auparavant, d'implémenter une fonction permettant de détecter si l'utilisateur entre une autre information qu'une 
date dans le *datepicker*.
    
    
    