Configuration
#############

Mise à jour et configuration basique
************************************

Avant de débuter la création de la station météorologique, il est important de bien configurer le Raspberry Pi. Commençons par mettre à jour Raspbian. Pour cela, exécutez la commande :

.. code-block:: sh 

   $ sudo apt-get update

Ensuite, pour mettre à niveau les paquets installés, entrez :

.. code-block:: sh

   $ sudo apt-get upgrade
    
Quand le système vous posera la question : *Do you want to continue [Y/n]*, pressez sur la touche **Y** puis **ENTER**. La mise à niveau prend ensuite quelques minutes à s'exécuter.

Pour la suite de la configuration, nous allons utiliser un outil très pratique, installé de base sur le système d'exploitation : ``raspi-config``. Pour le lancer, il suffit d'entrer :

.. code-block:: sh

    $ sudo raspi-config
    
Une fois effectuée, cette commande fait apparaître l'interface de ``raspi-config``. Nous allons réaliser trois changements :   

* Tout d'abord, nous allons lancer ``Expand Filesystem`` afin de nous assurer que le système d'exploitation a accès à l'entier de l'espace disponible sur la carte microSD. Comme l'action est déjà séléctionnée, pressez la touche **ENTER** 
  pour la lancer. Une fois terminée, un écran de confirmation devrait apparaître. Séléctionnez simplement *OK*.

* Les deux changements suivants se trouvent dans ``Advanced options``. Descendez dans l'interface avec les touches directionnelles du clavier et entrez dans les options avancées. Ouvrons en premier ``Memory split`` qui va
  permettre de modifier la mémoire dédiée au processeur graphique. En l'occurrence, comme notre Raspberry Pi ne dispose d'aucun écran, cette valeur peut être grandement diminuée. La valeur 8 est conseillée.

* Finalement, toujours dans ``Advanced options``, lancez ``I2C``. Cette opération est la première étape qui va permettre d'activer le lancement automatique du module *I2C*. Séléctionnez ``Yes`` à chaque fois qu'on vous le demande. Nous reparlerons des outils *I2C* 
  plus tard, sachez simplement pour l'instant qu'il s'agit d'un élément indispensable au fonctionnement des capteurs.
   
Pour terminer, quittez ``raspi-config`` en pressant deux fois sur la touche **TAB** pour séléctionner ``<Finish>``. Acceptez de réinitialiser le Raspberry Pi afin d'appliquer les changements.

Dongle Wi-Fi
************

Il est maintenant temps d'activer le Wi-Fi sur notre Raspberry Pi. Afin de se connecter, il va falloir indiquer le *SSID* ainsi que le mot de passe de votre Wi-Fi.
Pour cela, nous allons ouvrir un fichier de configuration avec *Nano* :

.. code-block:: sh

    $ sudo nano /etc/network/interfaces
    
Ce fichier contient les informations relatives au réseau. Nous allons effacer l'entier de son contenu et le remplacer par ceci :

.. code-block:: sh 
   :emphasize-lines: 9,10
   :linenos:

    auto lo

    iface lo inet loopback
    iface eth0 inet dhcp
    
    auto wlan0
    allow-hotplug wlan0
    iface wlan0 inet dhcp
    wpa-ssid "SSID"
    wpa-psk "mot_de_passe"
    
Remplacez les champs ``SSID`` ainsi que ``mot_de_passe`` par les identifiants de votre Wi-Fi. Pour fermer le fichier, pressez **CTRL+X** puis **Y** afin d'enregistrer les changements.
Réinitialisez ensuite le Raspberry Pi et débranchez le câble Ethernet pendant qu'il redémarre. Si tout s'est passé correctement, vous êtes
maintenant connecté par Wi-Fi. Recherchez la nouvelle adresse IP du Raspberry Pi avec *Angry IP Scanner* et reconnectez-vous avec *PuTTY* pour la suite du tutoriel.

Template du projet
##################

Pour organiser le projet, un template est proposé. Celui-ci contient la structure du code source. Ainsi, vous n'aurez qu'à vous soucier de le compléter en suivant les travaux 
pratiques. Créez-en un clone sur votre Raspberry Pi.

.. code-block:: sh
   
   $ git clone https://bitbucket.org/maxime_jan/template
   
L'organisation de ses dossiers est la suivante :

..  code-block:: sh

    `-- weather_station
        |-- client
        |   |-- html
        |   `-- static
        |       |-- css
        |       |-- js
        |       `-- libs
        `-- server

Ce template propose une architecture qui permet de rester organisé et de développer un code structuré dans lequel on se retrouve facilement. Le Raspberry Pi étant maintenant correctement configuré, 
vous êtes prêt à commencer le travail de création. 



