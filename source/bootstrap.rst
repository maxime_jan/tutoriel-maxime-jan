Créer les entrées utilisateur
#############################

La première étape qui va nous occuper du côté client est de mettre en place la création de graphiques. Pour cela, nous allons commencer par créer les entrées de l'utilsateur.

Comme annoncé dans l'introduction, nous utiliserons Bootstrap pour réaliser un design *responsive* ; cette 
étape arrivera à la fin du développement de la station météorologique. Cependant, nous allons maintenant en 
faire un emploi tout autre. Ce framework CSS et JavaScript permet en effet de facilement concevoir différents éléments personnalisables, comme des boutons, des questionnaires ou un outil de choix de typographie. Pour 
l'interface Web, nous allons incorporer un bouton permettant de déclencher la génération d'un graphique ainsi qu'un *range datepicker* avec lequel nous pourrons choisir la date de début et de 
fin du graphique. Nous aurons également besoin de checkbox permettant à l'utilisateur de choisir les types de données qu'il souhaite visualiser.

Bouton
******

Il est tout à fait possible de coder des boutons à la main. Il existe cependant un `outil`_ très pratique développé par bootsnipp.com qui permet de créer des boutons de manière interactive. Le code correspondant au bouton 
vous est donné. Il ne reste qu'à le coller dans le fichier *HTML* à l'endroit souhaité. Faites plusieurs essais et intégrez finalement un bouton en dessous de l'affichage des valeurs des capteurs.

Range datepicker
****************

Ajouter cet élément est un peu plus compliqué, mais reste tout de même grandement facilité grâce à une `application en ligne`_ avec laquelle il est possible de créer son *datepicker* personnalisé. Vous 
pouvez choisir les options que voulez et que vous jugez pertinentes pour la station météorologique. Vous devez néanmoins choisir le type *range* afin d'obtenir deux *datepicker* : un pour la date de début de l'intervalle et 
un autre pour la date de fin. Pour des questions de facilité, je vous conseille de choisir le format de date ``yyyy-mm-dd``. Celui-ci est en effet le format supporté par l'objet ``Date`` de JavaScript. Vous êtes évidemment  
libre d'en choisir un autre et de réaliser la conversion plus tard.

..  admonition:: note
    
    Seules les langues anglaises et françaises sont disponibles. Si vous désirez en utiliser une autre, rendez-vous dans le fichier ``/client/static/bootstrap/js/bootstrap-datepicker.js`` et ajoutez-la en copiant 
    le modèle d'une langue déjà installée.

Vous remarquerez qu'en plus des lignes *HTML*, du code JavaScript est également généré. Afin de ne pas encombrer les autres fichiers dans lesquels vous allez plus tard travailler, copiez ce code dans ``static/js/datepicker.js``.

Checkbox
********

Vous n'aurez probablement pas de peine à implanter des checkbox ; une pour la température, une pour l'humidité et une pour la pression. Cependant il y a une contrainte. L'utilisateur ne doit pas pouvoir séléctionner plus de deux 
checkbox à la fois. En effet, un graphique en deux dimensions ne peut pas prendre en compte trois échelles différentes. Pour pallier à ce problème, insérez chaque checkbox dans une classe et ajoutez ce code JavaScript [#f1]_ :

..  code-block:: js

    var limit = 2;
    $('input.nomDeLaClasse').on('change', function(evt) {
        if ($(this).siblings(':checked').length >= limit) {
            this.checked = false;
        }
    });

.. figure:: figures/EU.png
   :align: center
   :scale: 50%

   Les entrées utilisateur

Voilà un premier pas en direction de la création de graphiques : les entrées de l'utilisateur sont en place.
   



.. _outil: http://bootsnipp.com/buttons

.. _application en ligne: http://eternicode.github.io/bootstrap-datepicker

.. [#f1] Code source pris de : http://stackoverflow.com/questions/19001844/how-to-limit-the-number-of-selected-checkboxes