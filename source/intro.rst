#############################################
Une station météorologique sur Raspberry Pi 2
#############################################
    
Définition du projet
********************

Dans ce tutoriel, nous allons apprendre à développer une station météorologique avec différents capteurs sur le nano-ordinateur Raspberry Pi 2. Celle-ci possédera une interface Web 
pour visualiser ses données. Afin de vous donner une vue d'ensemble du projet ainsi que pour vous motiver à suivre ce tutoriel, une vidéo de présentation vous est proposée.

..  only:: html

    .. raw:: html
    
        <center><iframe align="center" width="560" height="315" src="https://www.youtube.com/embed/XU4RD-3c-Tc" frameborder="0" allowfullscreen></iframe></center>
    
.. only:: latex

   .. image:: figures/QRPRES.png
      :scale: 20%
      :align: center

   ..  raw:: latex
   
       ~
       \begin{center}
       QR code de la vidéo : https://www.youtube.com/watch?v=XU4RD-3c-Tc
       \end{center}



Les enjeux du tutoriel
**********************

Vous vous demandez certainement quelles raisons pourraient vous pousser à suivre ce tutoriel. En plus d'un objet concret, ce cours vous apportera de nombreuses connaissances. Voici un aperçu des compétences que vous acquerrez au long de 
celui-ci.

The Internet of Things
......................

*L'Internet des objets* est un terme, apparaissant pour la première fois en 1999, qui fait référence à l'extension d'Internet à un objet du monde physique. Cette notion est actuellement l'un des sujets les plus brûlants
dans le domaine informatique. Beaucoup s'accordent à dire que c'est une dimension fondamentale d'Internet, si ce n'est l'Internet du futur. Il s'agit cependant d'une notion complexe. Il n'est pas aisé d'en apprendre ses concepts
et encore moins de créer un premier projet.

Avec cette station météorologique, nous sommes au cœur de ce sujet. Le Raspberry Pi nous permettra de facilement mettre en place tous les éléments nécessaires. Il est en effet très facile de connecter différents dispositifs 
(capteurs, caméras, moteurs, ...) sur ce nano-ordinateur grâce à ses ports *GPIO (General Purpose Input/Output)*. Le tutoriel vous guidera pas à pas dans son utilisation afin de réaliser votre première application de l'Internet des objets.


Développement d'une application Web
...................................
    
Avec plus d'un milliard de sites internet aujourd'hui en ligne, avoir la capacité d'en créer un soi-même est une aptitude dont on ne peut plus se passer. La station météorologique 
vous permettra de toucher à la fois à la programmation côté client et côté serveur. Ce projet est donc idéal pour vous familiariser avec les différents concepts et technologies d'une application Web moderne. 


Technologies côté client
,,,,,,,,,,,,,,,,,,,,,,,,

Pour consulter des pages Web, nous utilisons aujourd'hui de nombreux appareils différents (smartphones, tablettes tactiles, ordinateurs portables, ...) possédant chacun une taille d'écran spécifique. Il est donc important, pour le confort de l'utilisateur, que la page visitée puisse 
s'y adapter. Dans la vidéo de présentation, vous avez pu apercevoir son apparence sur un grand écran. Observez la figure :ref:`smp`.

.. _smp:

.. figure:: figures/station_meteo_petit_ecran.png
   :align: center
   :scale: 40%
   
   interface Web sur petit écran

On appelle ceci un design *responsive*. Ce procédé permet de réorganiser la disposition ainsi que la dimension des éléments en fonction de la taille de l'écran, afin de faciliter la lecture et la navigation sur de petits appareils.

Technologies côté serveur
,,,,,,,,,,,,,,,,,,,,,,,,,

Pour le serveur nous utiliserons *Node.js*, une technologie assez récente. Cette plateforme logicielle contient une bibliothèque *HTTP* et nous permettra ainsi de créer un serveur sans avoir recours à un logiciel externe tel qu'Apache. De plus, 
*Node.js* exécute le code de manière non bloquante (asynchrone). Grâce à ce fonctionnement, les applications *Node.js* sont très rapides et idéales pour du temps réel.

Il est également essentiel pour une application Web de posséder une base de données. Celle-ci est indispensable pour enregistrer des informations de façon durable. Nous en mettrons une en place dans le projet afin 
de sauvegarder les valeurs des capteurs et de les récupérer lors de la création de graphiques.

Tout cela et bien plus encore sera entraîné et directement mis en situation concrète au cours du tutoriel.

Prérequis
*********

Ce cours requiert un nombre de connaissances élevé. Il est donc primordial qu'avant de vous y lancer, vous ayez acquis les bases de ces technologies. Il n'est cependant pas nécessaire d'étudier les sujets en 
profondeur mais d'en avoir simplement une compréhension basique. Vous recevrez en effet de l'aide, lors des travaux pratiques, qui vous guidera dans le processus de création de la station météorologique ainsi 
que des corrections commentées pour vous dépanner.

Vous trouverez ci-dessous la liste des langages et blibliothèques à connaître. Un tutoriel est à chaque fois proposé. Cependant, comme de nombreux autres cours traitant de ces sujets sont disponibles sur Internet, vous êtes libre d'en choisir 
un autre.

* **JavaScript** Ce sera le seul langage de programmation utilisé, pour le serveur et le client. Il s'agit du huitième langage le plus populaire au monde [#f1]_ et est absolument 
  incontournable dans le développement Web. Vous pouvez suivre le `cours JavaScript Éloquent`_ afin d'en acquérir les bases, ou le `cours d'Udacity`_ si vous êtes à l'aise avec l'anglais et préférez un format 
  vidéo.
* **Node.js + Express.js** Comme indiqué auparavant, nous utiliserons ces deux technologies pour la création du serveur. La lecture des deux premiers chapitres du `cours Node.js`_ d'OpenClassrooms vous apportera les connaissances nécessaires.
* **Web socket** Cette technologie permettra la communication entre le serveur et le client. Un `tutoriel`_ est proposé sur OpenClassrooms.
* **HTML/CSS** sont indispensables à la création d'une page Web. Si avez besoin de les apprendre, vous pouvez lire le `cours HTML et CSS`_ d'OpenClassrooms.
* **JQuery** nous permettra de modifier l'interface Web en temps réel. Vous pouvez suivre les treize premiers chapitres du `cours Jquery`_ sur Code Academy .
* **Bootstrap** C'est grâce à ce framework CSS que vous allez pouvoir réaliser un design *responsive*. Il permettra également d'améliorer l'apparence du site. 
  La lecture du `tutoriel Bootstrap`_ sur OpenClassrooms vous donnera les compétences nécessaires à son utilisation.


.. _cours JavaScript Éloquent: http://fr.eloquentjavascript.net/

.. _cours d'Udacity: https://www.udacity.com/course/javascript-basics--

.. _tutoriel: https://openclassrooms.com/courses/des-applications-ultra-rapides-avec-node-js/socket-io-passez-au-temps-reel

.. _cours HTML et CSS: https://openclassrooms.com/courses/apprenez-a-creer-votre-site-web-avec-html5-et-css3

.. _cours Node.js: https://openclassrooms.com/courses/des-applications-ultra-rapides-avec-node-js

.. _cours JQuery: https://www.codecademy.com/fr/learn/jquery

.. _tutoriel Bootstrap: https://openclassrooms.com/courses/prenez-en-main-bootstrap

Technologies expliquées dans le tutoriel
****************************************

Le tutoriel traite également d'autres technologies qui vous seront enseignées, étant donné que celles-ci ne disposent que de peu de documentation sur le Web.

* **Lokijs** Nous utiliserons cette base de données orientée documents afin d'enregistrer les données des capteurs. Sa petite taille et sa prise en main rapide en font un outil idéal pour ce projet. 
* **Google Charts** Il s'agit d'une *API* dans le *cloud* mis à disposition par Google. Celle-ci permet de créer de beaux graphiques dynamiques de manière intuitive. Elle permettra donc d'obtenir de très bons
  résultats pour un temps d'apprentissage moindre.
* **Node-grovepi** Il s'agit d'un module créé spécialement dans le but de pouvoir utiliser les capteurs que nous emploierons avec *Node.js*.




.. [#f1] d'après les statistiques de TIOBE Software en janvier 2016 <http://www.tiobe.com/index.php/content/paperinfo/tpci/index.html>