Réalisation de graphiques avec Google Charts
############################################

Présentation de l'API
*********************

L'*API* Google Visualization est un outil très puissant mis gratuitement à disposition des développeurs. Celle-ci permet de réaliser toute sorte de graphiques. Du diagramme en cercle au graphique en barre en passant par le 
diagramme de Gantt, l'*API* en propose un très grand nombre. Cet outil a en plus l'avantage d'être facile d'utilisation. La `documentation officielle`_ offre toutes les connaissances requises à son utilisation.

Il faut 
noter que Google Charts est un service dans le *cloud*. C'est-à-dire que le code source de l'*API* n'est pas disponible et ne peut pas être téléchargé. Il faut donc le charger à chaque fois en début d'exécution depuis les serveurs 
de Google. Ceci pose un inconvénient, bien que mineur. Il faudra en effet obligatoirement que le Raspberry Pi soit connecté à Internet pour charger l'*API*. En temps normal, le Raspberry Pi est de toute manière connecté à internet 
pour établir la connexion SSH. Cependant si vous utilisez par exemple votre téléphone portable pour vous connecter en SSH et que ce dernier ne possède pas de données mobiles, l'utilisation de Google Charts sera impossible. Si 
cela est le cas, je vous invite à changer de méthode pour la suite du tutoriel.


Utilisation des graphiques en courbe
************************************

Afin d'afficher l'évolution des mesures en fonction du temps, le type de graphique le mieux adapté est celui en courbe. Nous allons commencer par faire quelques tests dans notre projet afin d'en comprendre 
le fonctionnement. Comme mentionné dans la présentation, il faut tout d'abord importer l'*API* Google Visualization dans le projet. Cette étape est déjà effectuée dans le fichier template ``index.html`` :

..  captionup:: index.html
..  code-block:: html
    :lineno-start: 17
    
    <!-- Import de l'API Google Visualization-->
    <script type="text/javascript"
    src="https://www.google.com/jsapi?autoload={
    'modules':[{
    'name':'visualization',
    'version':'1',
    'packages':['corechart']
    }]
    }"></script>
    
    
Dans ce fichier, vous pouvez également observer une balise ``div`` comportant l'identifiant ``chart`` ; c'est dans celle-ci que nous insérerons le graphique. Rendons-nous maintenant dans ``graph.js``. Ici se trouve déjà une 
fonction ``drawChart(dataList)``. Vous l'aurez probablement compris, ``dataList`` correspond à la liste de données que nous avons récupérée lors du dernier TP. Ainsi, la fonction peut être activée de la manière suivante.

.. captionup:: graph.js
.. code-block:: js
    :emphasize-lines: 2
    
    socket.on("returnData", function(data) {
        google.setOnLoadCallback(drawChart(data));
    });

Retournons dans la fonction ``drawChart`` afin de générer un premier graphique. Il faut commencer par créer une instancier un objet de type ``DataTable``. Grâce à celui-ci, il est possible de définir les axes avec sa 
méthode ``addColumn("type_de_données", "nom_de_l_axe")``. La première utilisation de cette fonction déterminera l'axe X et les suivantes les différents axes Y. L'implantation se fait 
comme ceci :

.. captionup:: graph.js
.. code-block:: js
    
    function drawChart(dataList) {
        var dataTable = new google.visualization.DataTable();
        dataTable.addColumn("type", "nom_axe_X");
        dataTable.addColumn("type", "nom_axe_Y1");
        dataTable.addColumn("type", "nom_axe_Y2");
    }
    
Le paramètre ``type`` peut prendre trois valeurs différentes. Il peut s'agir de ``number``, ``text`` ou ``date``.  
Une fois les axes définis, il faut ajouter les valeurs que l'on souhaite afficher. Ceci se fait par l'intermédiare de la fonction ``addRow(list)``. Ce paramètre ``list`` sera évidemment ``dataList``. Cependant, 
comme le reste du graphique n'est pas configuré pour recevoir ces données, nous allons pour l'instant utiliser une liste factice. Pour un premier test, utilisons cet exemple montrant l'évolution sur six ans des moyennes au BAC du 
Collège du Sud [#f1]_ .

.. only:: latex

   ..  raw:: latex
   
       ~
       \begin{center}
       ~
       \end{center}

.. captionup:: graph.js
.. code-block:: js
    
        dataTable.addColumn("date", "Années");
        dataTable.addColumn("number", "moyenne générale des élèves du CSUD au BAC");
        dataTable.addRow([
            [new Date(2010), 4.78],
            [new Date(2011), 4.64],
            [new Date(2012), 4.80],
            [new Date(2013), 4.71],
            [new Date(2014), 4.54],
            [new Date(2015), 4.91]
        ])
    
Finalement, il ne reste plus qu'à créer le graphique en l'insérant dans la ``div`` souhaitée et à le dessiner :

.. captionup:: graph.js
.. code-block:: js
    
    var chart = new google.visualization.LineChart(document.getElementById("chart"));
    chart.draw(dataTable, options);
    
Vous pouvez constater que la fonction ``draw`` prend un objet ``options`` comme deuxième paramètre. Celui-ci, déjà défini au bas du fichier template, est paramétré afin d'afficher les dates de manière optimale et de 
pouvoir accueilir deux échelles de grandeur sur l'axe Y. Ne le changez pas pour le moment, vous vous en occuperez lors du prochain TP.

Le code minimal pour créer un graphique en courbe est maintenant en place, vous pouvez alors essayer l'exécuter. Si vous avez bien suivi l'exemple, lorsque vous cliquez sur le bouton vous devriez avoir le résultat de la 
figure :ref:`graphBAC`.

.. _graphBAC:

.. figure:: figures/graphBAC.png
   :align: center
   
   test de graphiques en lignes
   
Comme vous pouvez le remarquer, Google Charts ne requiert que peu de code pour obtenir de très beaux résultats !
   
Exercice
********

Afin de mieux vous familiariser avec ces graphiques en courbe, vous pouvez réaliser cet exercice. Il s'agira de simplement reproduire le graphique de la figure :ref:`GraphExo`. Cela ne devrait pas vous poser de problèmes. Notez 
cependant que l'axe Y prend deux échelles différentes : il faudra donc créer 3 colonnes.

.. _graphExo:

.. figure:: figures/graphExo.png
   :align: center
   
   graphique d'exercice
   

Quiz
****

..  quiz:: quiz1
    :question: "addColumn permet d'inclure des données dans un graphique"
    :answers: ["Vrai", "Faux"]
    :correction: [False, True]
    :genre: radio
    
..  quiz:: quiz1
    :question: "La taille des listes contenant les données doit être égale au nombre de colonnes"
    :answers: ["Vrai", "Faux"]
    :correction: [True, False]
    :genre: radio


.. only:: latex

   ..  raw:: latex
   
       ~
       \begin{center}
       ~
       \end{center}
       ~
       \begin{center}
       ~
       \end{center}
       ~
       \begin{center}
       ~
       \end{center}
       

..  quiz:: quiz1
    :question: "Les premiers éléments des listes contenant les données se placeront sur l'axe X"
    :answers: ["Vrai", "Faux"]
    :correction: [True, False]
    :genre: radio
    :btn_correction: quiz1



.. _documentation officielle : https://developers.google.com/chart/interactive/docs/

.. [#f1] Ces données sont purement fictives.