Nom : Jan

Prénom : Maxime

Adresse : Chemin du Closel 27, 1699 Bouloz

1.	Je certifie que le travail a été réalisé par moi conformément au Guide de travail des collèges et aux Lignes directrices de la DICS concernant la réalisation du Travail de maturité. 
2.	Je prends connaissance que mon travail sera soumis à une vérification de la mention correcte et complète de ses sources, au moyen d’un logiciel de détection de plagiat. Pour assurer ma protection, ce logiciel sera également utilisé pour comparer mon travail avec des travaux écrits remis ultérieurement, afin d’éviter des copies et de protéger mon droit d’auteur. En cas de soupçon d’atteintes à mon droit d’auteur, je donne mon accord à la direction de l’école pour l’utilisation de mon travail comme moyen de preuve. 
3.	Je m'engage à ne pas rendre public mon travail avant l'évaluation finale. 
4.	Je m’engage à respecter la Procédure d’archivage des TM en vigueur dans mon école. 
5.	J’autorise la consultation de mon travail par des tierces personnes à des fins pédagogiques et/ou d’information interne à l’école : 
    ❏ Oui 
    ❏ Non (car il contient des données personnelles et sensibles.)

Lieu, date : ________________________________________________________________

Signature : _______________________________________________________________