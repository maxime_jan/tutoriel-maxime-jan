Connexion SFTP
##############

Comme vous pouvez l'imaginer, développer une application Web dans une interface console n'est pas du tout pratique. La vidéo suivante vous apprendra donc à mettre en place une connexion SFTP avec votre Raspberry Pi afin de 
pouvoir modifier ses fichiers avec votre éditeur de texte favori.

..  only:: html

    .. raw:: html
    
       <center><iframe width="560" height="315" src="https://www.youtube.com/embed/lfuZPG6mV88" frameborder="0" allowfullscreen></iframe></center>
    
.. only:: latex

   ..  raw:: latex
   
       ~
       \begin{center}
       ~
       \end{center}
       ~
       \begin{center}
       ~
       \end{center}

   

   .. image:: figures/QRSFTP.png
      :scale: 20%
      :align: center

   ..  raw:: latex
   
       ~
       \begin{center}   
       QR code de la vidéo : https://www.youtube.com/watch?v=lfuZPG6mV88
       \end{center}
