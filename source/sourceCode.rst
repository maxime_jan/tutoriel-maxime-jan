Code source
###########

Lien du dépôt Bitbucket : https://bitbucket.org/maxime_jan/tm-maxime-jan

..  captionup:: NodeServer.js
..  code-block:: js
    :linenos:
        
    //Chargement des modules pour le serveur Node
    var express = require('express');
    var app = express();
    var server = require('http').createServer(app);
    var io = require('socket.io').listen(server);
    var fs = require('fs');
    var GrovePi = require("./sensors");
    var port = 8080;
    
    //Création du serveur http
    app.get('/', function(req, res) {
        res.sendFile("/home/pi/tm-maxime-jan/source/client/html" + '/index.html');
    });
    
    //Définition d'un fichier statique
    app.use(express.static("/home/pi/tm-maxime-jan/source/client/static"));
    
    //Mise en écoute du serveur sur le port indiqué
    server.listen(port, function() {
        console.log("Le serveur a été démarré sur le port " + port);
        GrovePi.startGP();
    });
    
    //Ecoute de socket.io dans le fichier "socketio.js"
    var socketIOFile = require("./socketio")(io);

.. captionup:: socketio.js
.. code-block:: js
    :linenos:

    var GrovePi = require("./sensors");
    var dataHandler = require("./dataHandler");
    var currentDHTValue;
    var currentBarometerValue;
    
    exports = module.exports = function(io) {
        //Evénènement déclenché à chaque nouvelle connection d'un client
        io.sockets.on("connection", function(socket) {
    
            socket.emit("GetDHTValue", currentDHTValue);
            socket.emit("GetDHTValue", currentDHTValue);
            //Evenement éxécuté lors d'un clique de l'utilisateur sur le bouton
            "Générer le graphe"
            socket.on("requestData", function(startDate, endDate, value) {
                dataHandler.RequestData(startDate, endDate, value);
                socket.emit("returnData", data);
            });
    
            GrovePi.dhtEvent.on("dhtSensorChange", function(res) {
                socket.emit("GetDHTValue", currentDHTValue);
            });
    
            GrovePi.barometerEvent.on("barometerSensorChange", function(res) {
                socket.emit("GetBarometerValue", currentBarometerValue);
            });
        });
    }
    setInterval(function() {
        if (currentDHTValue && currentBarometerValue) {
            dataHandler.SaveData(currentDHTValue, currentBarometerValue);
        }
    }, 60000);
    
    GrovePi.dhtEvent.on("dhtSensorChange", function(res) {
        currentDHTValue = res;
    });
    
    GrovePi.barometerEvent.on("barometerSensorChange", function(res) {
        currentBarometerValue = res;
    });

.. captionup:: dataHandler.js
.. code-block:: js
    :linenos:

    //Import de la base de données LokiJS
    var LokiDB = require('lokijs');
    //Création de l'objet de la base de données en précisant son emplacement
    var database = new LokiDB("LokiDatabase.json");
    var measurements;
    
    database.loadDatabase({}, function() {
        measurements = database.getCollection("measurements");
        if (measurements === null) {
            measurements = database.addCollection("measurements");
        }
    });
    
    function SaveData(currentDHTValue, currentBarometerValue) {
        var currentDate = new Date();
        console.log("Enregistrement d'une donnée");
        if (measurements != null) {
            measurements.insert({
                temperature: currentDHTValue[0],
                humidity: currentDHTValue[1],
                pressure: currentBarometerValue,
                date: currentDate.getTime()
            });
        }
        database.saveDatabase();
    }
    
    function RequestData(startDate, endDate, values) {
        data = [];
        day = 86400000;
        hour = 3600000;
        endDate += day;
        endDate -= hour;
        startDate -= hour;
    
        var requestedData = measurements.find({
            '$and': [{
                date: {
                    '$gt': startDate
                }
            }, {
                date: {
                    '$lt': endDate
                }
            }]
        });
    
        if (include(values, "Température") && include(values, "Humidité")) {
            for (var i = 0; i < requestedData.length; i++) {
    
                data.push([requestedData[i].date, requestedData[i].temperature, 
                requestedData[i].humidity]);
            }
        } else if (include(values, "Température") && include(values, "Pression")) {
            for (var i = 0; i < requestedData.length; i++) {
    
                data.push([requestedData[i].date, requestedData[i].temperature,
                requestedData[i].pressure]);
            }
        } else if (include(values, "Humidité") && include(values, "Pression")) {
            for (var i = 0; i < requestedData.length; i++) {
    
                data.push([requestedData[i].date, requestedData[i].humidity, 
                requestedData[i].pressure]);
            }
        } else if (include(values, "Température")) {
            for (var i = 0; i < requestedData.length; i++) {
                data.push([requestedData[i].date, requestedData[i].temperature]);
            }
        } else if (include(values, "Humidité")) {
            for (var i = 0; i < requestedData.length; i++) {
    
                data.push([requestedData[i].date, requestedData[i].humidity]);
            }
        } else {
            for (var i = 0; i < requestedData.length; i++) {
    
                data.push([requestedData[i].date, requestedData[i].pressure]);
            }
        }
        return data;
    }
    
    function include(list, item) {
        return (list.indexOf(item) != -1);
    }
    
    exports.SaveData = SaveData;
    exports.RequestData = RequestData;

..  captionup:: sensors.js
..  code-block:: js
    :linenos:

    //Import du module node-grovepi
    var GrovePi = require('node-grovepi').GrovePi;
    //Evenements
    var events = require('events');
    var dhtEvent = new events.EventEmitter();
    var barometerEvent = new events.EventEmitter();
    //Chargement des commandes du GrovePi
    var Commands = GrovePi.commands;
    //Chargement de la carte "GrovePi"
    var Board = GrovePi.board;
    var board;
    var dhtSensor;
    var barometerSensor;
    //Capteur de température et d'humidité relative
    var DHTDigitalSensor = GrovePi.sensors.DHTDigital;
    //Baromètre
    var BarometerI2CSensor = GrovePi.sensors.BarometerI2C;
    
    var start = function() {
            console.log('starting');
            //Création de l'objet "board" qui correspond au GrovePi+
            board = new Board({
                debug: true,
                onError: function(err) {
                    console.log('TEST ERROR');
                    console.log(err);
                },
    
                onInit: function(res) {
                    if (res) {
                        //Création de l'objet du capteur de température et humidité
                        dhtSensor = new DHTDigitalSensor(4, DHTDigitalSensor.VERSION.DHT22,
                        DHTDigitalSensor.CELSIUS);
                        //Création de l'objet de l'objet du baromètre
                        barometerSensor = new BarometerI2CSensor(BarometerI2CSensor);
                        // Temps en millisecondes
                        dhtSensor.watch(100);
                        barometerSensor.watch(100);
    
                        dhtSensor.on("change", function(res) {
                            dhtEvent.emit("dhtSensorChange", res);
                        })
    
                        barometerSensor.on("change", function(res) {
                            barometerEvent.emit("barometerSensorChange", res);
                        })
                    } else {
                        console.log('TEST CANNOT START');
                    }
                }
            })
            //Initialisation du GrovePi
            board.init()
        }
        //Fonction permettant d'arrêter le GrovePi
    
    function onExit(err) {
        console.log('ending');
        board.close();
        process.removeAllListeners();
        process.exit();
        if (typeof err != 'undefined')
            console.log(err);
    }
    process.on('SIGINT', onExit);
    
    exports.startGP = start;
    exports.dhtEvent = dhtEvent;
    exports.barometerEvent = barometerEvent;
    

..  captionup:: index.html
..  code-block:: html
    :linenos:
    
    <html>
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <!-- Import de socket.io -->
            <script src="js/socket.io-1.3.7.js"></script>
            <!-- Import de JQuery -->
            <script src="js/jquery-2.1.4.min.js"></script>
            <!-- Import de tous les fichiers relatifs à Bootstrap -->
            <script src="libs/bootstrap/js/bootstrap-datepicker.js"></script>
            <link href="libs/bootstrap/css/bootstrap.min.css" rel="stylesheet">
            <link href="libs/bootstrap/css/datepicker.css" type="text/css"
            rel="stylesheet">
            <link href="libs/bootstrap/css/bootstrap-glyphicons.css" rel="stylesheet">
            <!-- Lien vers la feuille CSS -->
            <link href="css/style.css" rel ="stylesheet">
            <script type="text/javascript"
            src="https://www.google.com/jsapi?autoload={
            'modules':[{
            'name':'visualization',
            'version':'1',
            'packages':['corechart']
            }]
            }"></script>
            <!-- Script comportant la majorité des websockets et de événements de la
            page -->
            <script src = "js/realTimeValue.js"> </script>
            <!-- Script contenant le datepicker -->
            <script src = "js/datepicker.js"></script>
            <!-- Script traitant de la création des graphiques -->
            <script src ="js/graph.js"></script>
            <!-- Script traitant de la température maximale et minimale -->
            <script src ="js/maxMin.js"></script>
        </head>
        <body>
            <!-- Container contenant tous les éléments Boostrap -->
            <div class="container">
                <div class ="white" >
                    <h1>
                    Station météo
                    </h1>
                    <div class="row">
                        <div class="col-sm-offset-4 col-sm-5 col-xs-12">
                            <div class = "row">
                                <p class ="col-xs-7 valueLabel">Valeurs du</p>
                                <div class = "col-xs-5 text_value" id="currentDate"></div>
                                
                                <p class ="col-xs-7 valueLabel">Température</p>
                                <div class = "col-xs-5 text_value" id="temperature"></div>
                            </div>
                        </div>
                        <div class="col-sm-offset-3"></div>
                        <div class="col-sm-offset-4 col-sm-5 col-xs-12">
                            <div class = "row">
                                <p class ="col-xs-7 valueLabel">Humidité relative</p>
                                <div class = "col-xs-5 text_value" id="humidity"></div>
                                
                                <p class ="col-xs-7 valueLabel">Pression atmosphérique</p>
                                <div class = "col-xs-5 text_value" id="pressure"></div>
                            </div>
                        </div>
                        <div class="col-sm-offset-3"></div>
                    </div>
                    <!-- Datepicker -->
                    <div class ="row">
                        
                        <div class="col-sm-offset-4 col-lg-4 col-sm-6 col-xs-12">
                            <div class="input-daterange input-group" id="datepicker">
                                <span class="input-group-addon">du</span>
                                <input type="text" class="input-sm form-control"
                                name="start" id="start"/>
                                <span class="input-group-addon">au</span>
                                <input type="text" class="input-sm form-control"
                                name="end" id="end" />
                            </div>
                            <div class="pricing-levels-3">
                                <p class="valueLabel">Types de données dans le 
                                graphique :</p>
                                <input class="single-checkbox"type="checkbox"
                                id="Température" checked>Température (C)<br>
                                <input class="single-checkbox" type="checkbox"
                                id="Humidité" checked>Humidité relative (%)<br>
                                <input class="single-checkbox" type="checkbox"
                                id="Pression">Pression atmosphérique (hPa)<br>
                            </div>
                        </div>
                    </div>
                    <!--bouton pour générer un graphe-->
                    <div class="row">
                        <div class = "col-sm-offset-4 col-sm-3 col-xs-5">
                            <a id ="graph" class="btn btn-primary btn-success">
                            <span class="glyphicon glyphicon-chevron-right">
                            </span>Générer le graphique</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class = "col-sm-offset-4 col-sm-6 col-xs-12"><b>
                            <div class="alert alert-danger" id ="error">
                            </div>
                        </b></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-4 col-xs-12">
                            <div class = "row">
                                <p class ="col-sm-8 col-xs-7 valueLabel" id="max1Label"></p>
                                <div id="max1" class = "col-sm-4 col-xs-5 text_value"></div>
                                
                                <p class ="col-sm-8 col-xs-7 valueLabel" id="min1Label"></p>
                                <div id="min1" class = "col-sm-4 col-xs-5 text_value"></div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class = "row">
                                <p class ="col-sm-8 col-xs-7 valueLabel" id="max2Label"></p>
                                <div id="max2" class = "col-sm-4 col-xs-5 text_value"></div>
                                
                                <p class ="col-sm-8 col-xs-7 valueLabel" id="min2Label"></p>
                                <div id="min2" class = "col-sm-4 col-xs-5 text_value"></div>
                            </div>
                        </div>
                        <div class="col-sm-offset-2"></div>
                    </div>
                    <!-- Graphe-->
                    <div class ="row">
                        <div class="col-xs-12">
                            <div id="chart"></div>
                        </div>
                    </div>
                </div>
            </div>
        </body>
    </html>

..  captionup:: realTimeValue.js
..  code-block:: js
    :linenos:
    
    //Connection au serveur
    var socket = io.connect(window.location.host);
    //évènement qui récupère les données du capteur DHT
    socket.on("GetDHTValue", function(data) {
        //Création de la date actuelle
        var today = new Date();
        if (data[0] != undefined && data[1] != undefined) {
            $("#currentDate").text(today.toLocaleString());
            $("#temperature").text(data[0] + "°C");
            $("#humidity").text(data[1] + "%");
        }
    });
    
    socket.on("GetBarometerValue", function(data) {
        //Création de la date actuelle
        var today = new Date();
        if (data != 0) {
            $("#currentDate").text(today.toLocaleString());
            $("#pressure").text(data + " hPa")
        }
    });

    
..  captionup:: graph.js
..  code-block:: js
    :linenos:
    
    function include(list, item) {
        return (list.indexOf(item) != -1);
    }
    //Fonction qui dessine le graphe
    function drawChart(dataList) {
        for (var i = 0; i < dataList.length; i++) {
            dataList[i][0] = new Date(dataList[i][0]);
        }
        color = [];
        column2 = "";
        title1 = "";
        title2 = "";
        mainTitle = "";
    
        if (include(values, "Température") && include(values, "Humidité")) {
            title1 = "Température (C)";
            title2 = "Humidité relative (%)";
            color.push('#ff0000');
            color.push('#006bff');
            mainTitle = "Evolution de la tempértaure et de l'humidité relative"
        } else if (include(values, "Température") && include(values, "Pression")) {
            title1 = "Température (C)";
            title2 = "Pression atmosphérique (hPa)";
            color.push('#ff0000');
            color.push('#b89502');
            mainTitle = "Evolution de la tempértaure et de la pression atmosphérique"
        } else if (include(values, "Humidité") && include(values, "Pression")) {
            title1 = "Humidité relative (%)";
            title2 = "Pression atmosphérique (hPa)";
            color.push('#006bff');
            color.push('#b89502');
            mainTitle = "Evolution de l'humidité relative et de la pression atmosphérique"
        } else if (include(values, "Humidité")) {
            title1 = "Humidité relative (%)";
            color.push('#006bff');
            mainTitle = "Evolution de l'humidité relative";
        } else if (include(values, "Température")) {
            title1 = "Température (C)";
            color.push('#ff0000');
            mainTitle = "Evolution de la tempértaure";
        } else {
            title1 = "Pression atmosphérique (hPa)";
            color.push('#b89502');
            mainTitle = "Evolution de la pression atmosphérique";
        }
    
        var dataTable = new google.visualization.DataTable();
    
        options.colors = color;
        options.title = mainTitle;
    
        dataTable.addColumn('date', 'Temps');
        if (title2) {
            options.vAxes = {
                0: {
                    title: title1
                }, //Grâce à l'indexisation, on peut nommer les axes Y
                1: {
                    title: title2
                }
            };
            dataTable.addColumn('number', title1);
            dataTable.addColumn('number', title2);
        } else {
            options.vAxes = {
                0: {
                    title: title1
                }
            };
            dataTable.addColumn('number', title1);
        }
        //On ajoute nos données
        dataTable.addRows(dataList);
        //Div dans laquelle on veut insérer le graphe
        var chartDiv = document.getElementById('chart');
        //On insère le graphe dans la div choisie 
        var chart = new google.visualization.LineChart(chartDiv);
        //Dessin du graphe
        chart.draw(dataTable, options);
        //Fonction permettant de redessiner le graphe à chaque fois que la fenêtre est
        redimensionnée
        //http://stackoverflow.com/questions/18979535/make-google-chart-gauge-responsive
        function resizeHandler() {
            chart.draw(dataTable, options);
        }
        if (window.addEventListener) {
    
            window.addEventListener('resize', resizeHandler, false);
        } else if (window.attachEvent) {
    
            window.attachEvent('onresize', resizeHandler);
        }
    }
    
    var start, end, values;
    $(function() {
        $('#graph').on('click', function() {
            //http://stackoverflow.com/questions/11810874/how-to-get-an-input-text-value-
            in-javascript
            start = document.getElementById("start").value;
            end = document.getElementById("end").value;
            values = [];
    
            if (document.getElementById("Température").checked) {
                values.push("Température");
            }
            if (document.getElementById("Humidité").checked) {
                values.push("Humidité");
            }
            if (document.getElementById("Pression").checked) {
                values.push("Pression");
            }
    
            startDate = new Date(start);
            endDate = new Date(end);
    
            if (values.length != 0) {
                socket.emit("requestData", startDate.getTime(), endDate.getTime(), values);
            } else {
    
                ErrorSelect();
            }
        });
    });
    
    socket.on("returnData", function(data) {
        var unit;
        if (data.length != 0) {
            var min = FindLowest(data, values.length);
            var max = FindHighest(data, values.length);
    
            for (var i = 1; i < (values.length + 1); i++) {
    
                switch (values[i - 1]) {
                    case "Température":
                        unit = "°C";
                        break;
    
                    case "Humidité":
                        unit = "%";
                        break;
    
                    case "Pression":
                        unit = "hPa";
                        break;
                }
                $("#min" + i.toString() + "Label").text(values[i - 1] + " minimale");
                $("#min" + i.toString() + "Label").show();
                $("#min" + i.toString()).text(min[i - 1].toString() + unit);
                $("#min" + i.toString()).show();
    
                $("#max" + i.toString() + "Label").text(values[i - 1] + " maximale");
                $("#max" + i.toString() + "Label").show();
                $("#max" + i.toString()).text(max[i - 1].toString() + unit);
                $("#max" + i.toString()).show();
            }
    
            if (values.length == 1) {
                $("#min2Label").hide();
                $("#min2").hide();
                $("#max2Label").hide();
                $("#max2").hide();
            }
            $("#chart").show();
            $("#error").hide();
            google.setOnLoadCallback(drawChart(data));
        } else {
            HideMaxMinChart()
            $("#error").text("Il n'y a pas de données enregistrées pour l'intervalle
            séléctionné.");
            $("#error").show();
        }
    });
    
    window.setInterval(function() {
        var endDate = new Date(end);
        var now = new Date();
        var today = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 1);
        if (today.getTime() == endDate.getTime() && values.length != 0) {
            socket.emit("requestData", startDate.getTime(), endDate.getTime(), values);
        }
    }, 5000);
    
    function HideMaxMinChart() {
        $("#min1Label").hide();
        $("#min2Label").hide();
        $("#min1").hide();
        $("#min2").hide();
    
        $("#max1Label").hide();
        $("#max2Label").hide();
        $("#max1").hide();
        $("#max2").hide();
        $("#chart").hide();
    }
    
    function ErrorSelect() {
        HideMaxMinChart();
        $("#error").text("Veuillez séléctionner au moins un type de donnée à afficher dans
        le graphique.");
        $("#error").show();
    }
    
    var options = {
        //Permet d'avoir des courbes et non des traits tout droit
        curveType: 'function',
        hAxis: {
            gridlines: {
                count: -1,
                units: {
                    days: {
                        format: ['yyyy/MM/dd']
                    },
                    hours: {
                        format: ["MM/dd HH:00",
                            "HH:00"
                        ]
                    },
                    minutes: {
                        format: [
                            "HH:mm"
                        ]
                    },
                }
            },
            minorGridlines: {
                units: {
                    hours: {
                        format: ['hh:mm:ss']
                    },
                    minutes: {
                        format: ['HH:mm']
                    }
                }
            }
        },
        series: {
            0: {
                targetAxisIndex: 0
            }, //Permet d'indexer chaque axe Y
            1: {
                targetAxisIndex: 1
            }
        }
    };

    $(function() {
        $("#error").hide();
    });
    
..  captionup:: maxMin.js
..  code-block:: js
    :linenos:
    
    function FindLowest(dataList, size) {
        var min = [100000, 100000];
        for (var j = 0; j < size; j++) {
            for (var i = 0; i < dataList.length; i++) {
                if (dataList[i][j + 1] < min[j]) {
                    min[j] = dataList[i][j + 1];
                }
            }
        }
        return min;
    }
    
    function FindHighest(dataList, size) {
        var max = [-100000, -100000];
        console.log(size)
        for (var j = 0; j < size; j++) {
            for (var i = 0; i < dataList.length; i++) {
                if (dataList[i][j + 1] > max[j]) {
                    max[j] = dataList[i][j + 1];
                }
            }
        }
        return max;
    }
    
    $(function() {
        //http://stackoverflow.com/questions/19001844/how-to-limit-the-number-of-
        selected-checkboxes
        var limit = 2;
        $('input.single-checkbox').on('change', function(evt) {
            if ($(this).siblings(':checked').length >= limit) {
                this.checked = false;
            }
        });
    });
                

..  captionup:: datepicker.js
..  code-block:: js
    :linenos:

    $(document).ready(function() {
        $('.input-daterange').datepicker({
                    format : "yyyy-mm-dd",
                    todayBtn: "linked",
        			todayHighlight: true
                    });
    });
    

..  captionup:: style.css
..  code-block:: css
    :linenos:
    
    h1 {
    	text-align: center;
    	font-size: 42px;
    	margin-bottom: 5px;
    }
    #humidity, #temperature, #currentDate {
    	font-size: 14px;
    }
    .valueLabel{
    	font-size: 18px;
    	font-weight: bold;
    	margin-top: 15px;	
    }
    .text_value {
    	position: relative;
    	top: 20px;
    }
    .btnMaxMin {
    	position: relative;
    	bottom: 10px;
    	margin-top : 5px;
    }
    .valueMaxMin {
    	height: 30px;
    	position: relative;
    	bottom: 10px;
    	margin-top : 5px;
    }
    #chart{
    	width:100%;
    	height:50%;
    }
    #graph {
    	margin-top: 15px;
    	margin-bottom: 12px;
    }
    #error {
    	margin-top: 15px;
    }
    body {
    
    	background-color : rgb(220,220,220);
    }
    .white {
    
    	background-color: white;
    }
    
.. only:: latex

   .. raw:: latex
   
      \clearpage

      
        \title{Déclaration sur l’honneur}
        
        \textbf{Déclaration sur l’honneur}
        
        Nom : Jan
        
        Prénom : Maxime
        
        Adresse : Chemin du Closel 27, 1699 Bouloz
        
        Je certifie que le travail : "Créer sa propre station météorologique avec interface Web sur Raspberry Pi 2" a été réalisé par moi conformément au Guide de travail des collèges et aux Lignes directrices de la DICS concernant la réalisation du Travail de maturité. 
        
        Je prends connaissance que mon travail sera soumis à une vérification de la mention correcte et complète de ses sources, au moyen d’un logiciel de détection de plagiat. Pour assurer ma protection, ce logiciel sera également utilisé pour comparer mon travail avec des travaux écrits remis ultérieurement, afin d’éviter des copies et de protéger mon droit d’auteur. En cas de soupçon d’atteintes à mon droit d’auteur, je donne mon accord à la direction de l’école pour l’utilisation de mon travail comme moyen de preuve. 
        
        Je m'engage à ne pas rendre public mon travail avant l'évaluation finale. 
        
        Je m’engage à respecter la Procédure d’archivage des TM en vigueur dans mon école. 
        
        J’autorise la consultation de mon travail par des tierces personnes à des fins pédagogiques et/ou d’information interne à l’école : 
        
        \Box{} Oui
        
        \Box{} Non
        
        Lieu, date : ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Signature : 
        
