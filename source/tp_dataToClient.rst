TP : Envoyer des données au client en temps réel
################################################

Direction à prendre
*******************

Maintenant que vous avez appris à utiliser les capteurs et que le serveur est en place, vous allez pouvoir créer une première ébauche de la station météorologique. Pour cela, il s'agira d'envoyer les données que vous captez vers le client avec *socket.io* et d'utiliser *JQuery* afin de les mettre à jour en temps réel. 
Afin de vous aider dans votre travail, une marche à suivre vous sera proposée lors de chaque TP. Celle-ci détaille les étapes nécessaires à la réalisation du travail. Cependant, il ne s'agit que d'une aide qui vous permet de ne 
pas vous perdre dans la tâche à réaliser. Il existe des dizaines d'autres méthodes - peut-être meilleures - que celle proposée. Ne prenez donc pas ces consignes comme une obligation mais plutôt comme une indication.

* Serveur

  * Démarrage du GrovePi au lancement du serveur.
  * Récupération des données dans le fichier ``socketio.js`` (cf. figure :ref:`eventemitter`).
  * Création des événements socket ``GetDHTValue`` et ``GetBarometerValue``.
  
* Client

  * Récupération des données dans le fichier ``realTimeValue.js``.
  * Actualisation des données en temps réel.
  * Actualisation de la date en temps réel.
 
La récupération des données dans ``socketio.js`` est l'étape la plus ardue. Il n'est en effet pas aisé de les récupérer à chaque changement. Il existe différentes manières de réaliser ceci. Cependant, la plus concise consiste à 
créer un ``EventEmitter``. Pour vous aider dans sa mise en place, observez la figure :ref:`eventemitter` qui vous indique les grandes lignes de ce processus. Celui-ci n'est cependant pas la méthode la plus facile. Si vous pensez à une autre solution 
qui vous semble plus abordable, n'hésitez pas à l'essayer.

.. _eventemitter:

.. figure:: figures/eventemitter.png
   :align: center
   :figwidth: 50%
   :scale: 50 %
   
   schéma de la mise en place d'un ``EventEmitter``
 
Si vous rencontrez des problèmes quant aux capteurs, n'hésitez pas à relire le chapitre précédent. Dans le cas où vous seriez complètement bloqué, jetez un œil à la correction.

Bon travail !

Corrections
***********

Si vous êtes arrivé au but, félicitations. Il est tout de même conseillé de lire les corrections. En effet, vous trouverez certainement des éléments qui vous faciliteront la suite. Si vous n'êtes pas arrivé au terme 
du TP, ne baissez pas les bras et essayez de trouver, dans les corrections, un élément qui vous débloquera.

Pour ces corrections et toutes celles à venir, vous trouverez les étapes détaillées une à une. Si en revanche vous préférez avoir le code source au complet, vous pouvez le consulter dans les annexes.

Serveur
.......

La première étape que l'on souhaite effectuer est de démarrer la carte GrovePi au lancement du serveur. Pour ce faire, il suffit d'exporter la fonction ``start``.

.. captionup:: sensor.js
.. code-block:: js
   
   exports.startGP = start;
   
Il ne reste ensuite plus qu'à importer cette fonction dans le fichier du serveur et d'y lancer la fonction. Afin d'éviter des problèmes plus tard, il est plus consciencieux d’exécuter  la fonction une fois le serveur prêt.

.. captionup:: NodeServer.js
.. code-block:: js
   
    var GrovePi = require("./sensors")
   
    //...
   
    server.listen(port, function() {
        console.log("Un serveur a été démarré sur le port" + port);
        GrovePi.startGP();
    });
    
La prochaine étape est d'implanter un ``EventEmitter`` afin de transférer les données depuis ``sensor.js`` vers ``socketio.js``. Le schéma qui vous était proposé vous donnait une première idée quant à la démarche. Commençons par le fichier 
``sensor.js``. La première chose à faire est d'instancier un ``EventEmitter`` pour chaque capteur et de les exporter. 

.. captionup:: sensor.js
.. code-block:: js
    
    var events = require('events');
    
    var dhtSensorEvent = new events.EventEmitter();
    var barometerSensorEvent = new events.EventEmitter();
    
    exports.dhtSensorEvent = dhtSensorEvent;
    exports.barometerSensorEvent = barometerSensorEvent;

Ces événements sont dès lors prêts à être utilisés. Nous voulons qu'ils se déclenchent à chaque changement de valeurs d'un des capteurs. Il faut donc les placer **à l'intérieur** de l'événement ``change``. ``res`` est passé comme argument 
afin de récupérer les valeurs dans ``socketio.js``.

.. captionup:: sensor.js
.. code-block:: js
    
    dhtSensor.on("change", function(res) {
        dhtEvent.emit("dhtSensorChange", res);
    });
    
    barometerSensorEventSensor.on("change", function(res) {
        barometerSensorEvent.emit("barometerSensorChange", res);
    });

On peut maintenant ouvrir le fichier ``socketio.js``. Il faut évidemment commencer par importer les fonctions de ``sensor.js``.

.. captionup:: socketio.js
.. code-block:: js
    
    var sensors = require("./sensors");
    
Nous pouvons mettre en place l'écoute de nos événements. Il faut bien faire attention à les placer **dans** la fonction de callback de `io.sockets.on` car un événement *socket* sera ensuite déclaré à l'intérieur.

.. captionup:: socketio.js
.. code-block:: js
    
    exports = module.exports = function(io) {

        io.sockets.on("connection", function(socket) {
        
            GrovePi.dhtEvent.on("dhtSensorChange", function(res) {
            });
            
            GrovePi.barometerEvent.on("barometerSensorChange", function(res) {
            });
        });
    }

Les événements sont maintenant installés. Vous pouvez essayer un ``console.log`` lors de leur déclenchement afin de voir si les données se mettent à jour correctement.

Un événement *socket* est ensuite créé pour chaque capteur, prenant ``res`` en paramètre, afin d'envoyer les données au client.

.. only:: latex

   ..  raw:: latex
   
       ~
       \begin{center}
       ~
       \end{center}
       
.. captionup:: socketio.js
.. code-block:: js

    GrovePi.dhtEvent.on("dhtSensorChange", function(res) {
        socket.emit("GetDHTValue", res);
    });
    
    GrovePi.barometerEvent.on("barometerSensorChange", function(res) {
        socket.emit("GetBarometerValue", res);
    });
    
Cependant, vous remarquerez peut-être une faiblesse dans ce code. En effet, les événements ne sont sur écoute qu'à partir de l'ouverture d'une connexion ``socket.io``. Si vous décidez d'ouvrir l'interface Web, vous n'obtiendrez donc aucune 
donnée jusqu'à ce qu'un capteur change de valeur et active l'événement *socket*. Ce disfonctionnement est plutôt embêtant. La meilleure solution pour y remédier est d'instancier une nouvelle écoute des événements à **l'extérieur** de la 
fonction de callback de ``socket.io`` et d'assigner la valeur de ``res`` à des variables créées au préalable. Nous serons ainsi certains que les données seront toujours à jour. On peut maintenant envoyer une première fois ces valeurs à l'ouverture 
d'une connexion avec un client afin d'être sûr d'afficher les données dès le chargement de la page.

.. captionup:: socketio.js
.. code-block:: js
    
    var currentDHTValue;
    var currentBarometerValue;

    io.sockets.on("connection", function(socket) {
        socket.emit("GetDHTValue", currentDHTValue)
        socket.emit("GetBarometerValue", currentBarometerValue)
        //...
    });
    
    GrovePi.dhtEvent.on('dhtSensorChange', function(res) {
        currentDHTValue = res;
    });
    
    GrovePi.barometerEvent.on('barometerSensorChange', function(res) {
        currentBarometerValue = res;
    });
    
.. admonition:: attention

    N'oubliez pas de changer le paramètre *res* des événements ``socket.io`` créés auparavant avec ces nouvelles variables.
    
Client
......

Ouvrez le fichier ``realTimeValue.js``. Comme vous l'avez certainement déjà constaté, la connexion ``socket.io`` avec le serveur est déjà effectuée. L'écoute des événements socket.io peut ainsi directement être mise en place.

.. captionup:: realTimeValue.js
.. code-block:: js
    
    socket.on("GetDHTValue", function(currentDHTValue) {
    });
    
    socket.on("GetBarometerValue", function(currentBarometerValue) {
    });
    
En étudiant le fichier ``index.html``, vous aurez probablement remarqué que des ``div`` portant un identifiant correspondant aux noms de nos données existant déjà. Afin de les mettre à jour en 
temps réel, il suffit de modifier leur texte à chaque déclenchement de l'événement grâce à *JQuery*.

.. captionup:: realTimeValue.js
.. code-block:: js
    
    socket.on("GetDHTValue", function(currentDHTValue) {
        $("#temperature").text(currentDHTValue[0] + "°C");
        $("#humidity").text(currentDHTValue[1] + "%");
    });
    
    socket.on("GetBarometerValue", function(currentBarometerValue) {
        $("#pressure").text(currentBarometerValue + "hPa");
    });
    
Pour ajouter la date, il suffit de créer un nouvel objet ``Date`` à chaque déclenchement des événements puis d'utiliser le même principe de suppression et d'ajout.

.. captionup:: realTimeValue.js
.. code-block:: js
    
    var currentDate = new Date();
    $("#currentDate").text(currentDate.toLocaleString());
    
Vous avez maintenant une première ébauche de la station météorologique. Celle-ci n'est cependant pas encore très intéressante. En effet, toutes ces valeurs défilant à l'écran ne sont pas enregistrées. C'est à cela que nous allons nous 
atteler dans le prochain sous-chapitre en apprenant à manier une base de données.