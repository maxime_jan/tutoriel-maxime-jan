Connexion SSH
#############

Maintenant que tout le matériel est connecté et prêt à fonctionner, il est temps de prendre le contrôle du Raspberry Pi. Suivez pour cela la vidéo suivante, dans laquelle vous apprendrez à établir une connexion SSH afin d'accéder à l'interface 
console du Raspberry Pi.

..  only:: html

    .. raw:: html
    
       <center><iframe width="560" height="315" src="https://www.youtube.com/embed/pJer3qFzcyw" frameborder="0" allowfullscreen></iframe></center>
    
.. only:: latex

   .. image:: figures/QRSSH.png
      :scale: 20%
      :align: center
      
   ..  raw:: latex
   
       ~
       \begin{center}
       QR code de la vidéo : https://www.youtube.com/watch?v=pJer3qFzcyw
       \end{center}

    
