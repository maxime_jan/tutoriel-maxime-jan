TP : Récupérer les données en fonction des entrées utilisateur
##############################################################

Direction à prendre
*******************

Maintenant que les entrées utilisateur sont en place, il est temps de les faire fonctionner ! Avant de pouvoir générer un graphique, il faut évidemment aller consulter la base de données. Celle-ci doit nous renvoyer
les valeurs séléctionées avec les checkbox dans l'intervalle de temps délimité par le *datepicker*. Cette action devra être déclenchée lorsque l'utilisateur clique sur le bouton.
Pour ce TP, rendez-vous dans le fichier ``graph.js``. Voici les étapes qui doivent s'enchaîner lorsque l'utilisateur active le bouton :

* Récupérer les valeurs des entrées utilisateur : ``startDate`` et ``endDate`` du *datepicker*, ainsi que ``values`` des checkbox.
* Déclencher un événement *socket* qui s'active dans ``socketio.js``.
* Exécuter la fonction ``RequestData(startDate, endDate, values)`` de ``dataHandler.js`` qui retourne une liste des données souhaitées. Vous devrez l'écrire au préalable.
* Déclencher un événement *socket* qui renvoie les données au client une fois que celles-ci ont été récupérées.

.. admonition:: attention

   La liste des données retournée au client ne peut pas prendre n'importe quelle forme. En effet, afin de pouvoir facilement les intégrer dans un graphique, celles-ci devront suivre le format suivant :
   
   liste : ``[[date, value1, value2], [date, value1, value2], [...]]``
   
   Si l'utilisateur ne demande qu'une seule valeur, la liste ne contiendra évidemment pas ``value2``.
   
Vous aurez probablement besoin de relire rapidement la partie concernant les requêtes de la base de données dans le chapitre traitant de *Loki.js*. Bon travail !

Corrections
***********

Passons à la correction. Cette fois également nous allons suivre les étapes proposées dans la marche à suivre. Comme la recherche de données doit s'exécuter à chaque fois que le bouton est activé, on utilise *JQuery* afin de détecter 
les cliques de l'utilisateur sur ce dernier.

.. captionup:: graph.js
.. code-block:: js

    $(function() {
        $('#bouton').on('click', function() {
            //...
        }
        
A chaque déclenchement de cette fonction, il faut récupérer les valeurs des entrées utilisateur. Pour le *datepicker* nous avons la date de début et de fin ; celles-ci doivent être converties en millisecondes. Comme la date est 
exprimée pour le jour même à 0h00, il faut rajouter 24h pour la date de fin, c'est-à-dire ``86400000ms``.

.. captionup:: graph.js
.. code-block:: js
    
        var start, end;
        start = document.getElementById("start").value;
        end = document.getElementById("end").value;
        
        startDate = new Date(start).getTime();
        endDate = new Date(end).getTime() + 86400000;
        
Afin de ne passer qu'une seule variable en paramètre pour les trois checkbox, nous entrons leur nom dans une liste si celles-ci ont été cochées.

.. captionup:: graph.js
.. code-block:: js
    
    var values = [];
    if(document.getElementById("Temp").checked){
        values.push("Temp");
    }
    if (document.getElementById("Hum").checked){
        values.push("Hum");
    }
    if (document.getElementById("Press").checked){
        values.push("Press");
    }
    


Puis nous pouvons lancer l'événement *socket*

.. captionup:: graph.js
.. code-block:: js
    
    socket.emit("requestData", startDate, endDate, value);

Regardons maintenant la fonction ``RequestData`` de ``dataHandler.js``. Dans celle-ci, il faut demander à la base de données de nous retourner toutes les mesures prises entre la date de début et la date de fin. Cette requête 
pourrait être exprimée de la sorte : ``date > startDate && date < endDate``. Pour *Loki.js*, cette expression est traduite de la manière suivante :

.. captionup:: dataHandler.js
.. code-block:: js
    
    var requestedData = measurements.find({
        '$and': [{
            date: {
                '$gt': startDate
            }
        }, {
            date: {
                '$lt': endDate
            }
        }]
    })
    
Ces données doivent maintenant être formatées selon le paramètre ``values``. Pour cela, chaque cas sera traité par des conditions. Afin d'effectuer ce contrôle, nous utilisons la fonction ``include(liste, item)`` [#f1]_ que nous devons 
définir au préalable. Celle-ci renvoie ``true`` si l'``item`` se trouve dans la liste.
Puis pour chaque cas, une boucle ``for`` de la taille du nombre de mesures récupérées est ensuite parcourue. A chaque itération, la liste 
``data`` est agrandie en suivant le modèle demandé dans la consigne. Puis cette liste peut être retournée.

.. captionup:: dataHandler.js
.. code-block:: js
    
    function include(list,item) {
        return (list.indexOf(item) != -1);
    }

    data = []
    
    if (include(values, "Température") && include(values, "Humidité")) {
        for (var i = 0; i < requestedData.length; i++) {
            data.push([requestedData[i].date, requestedData[i].temperature, 
            requestedData[i].humidity]);
        }
    } else if (include(values, "Température") && include(values, "Pression")) {
        for (var i = 0; i < requestedData.length; i++) {
            data.push([requestedData[i].date, requestedData[i].temperature, 
            requestedData[i].pressure]);
        }
    } else if (include(values, "Humidité") && include(values, "Pression")) {
        for (var i = 0; i < requestedData.length; i++) {
            data.push([requestedData[i].date, requestedData[i].humidity, 
            requestedData[i].pressure]);
        }
    } else if (include(values, "Température")) {
        for (var i = 0; i < requestedData.length; i++) {
            data.push([requestedData[i].date, requestedData[i].temperature]);
        }
    } else if (include(values, "Humidité")) {
        for (var i = 0; i < requestedData.length; i++) {
            data.push([requestedData[i].date, requestedData[i].humidity]);
        }
    } else {
        for (var i = 0; i < requestedData.length; i++) {
            data.push([requestedData[i].date, requestedData[i].pressure]);
        }
    }
    return data;


Finalement, rendons-nous dans le fichier ``socketio.js`` pour établir le lien entre ``graph.js`` et ``dataHandler.js``. Il faut tout d'abord mettre en écoute l'événement déclenché plus tôt et exécuter la fonction ``RequestData`` à 
l'intérieur.

.. captionup:: socketio.js
.. code-block:: js

    socket.on("requestData", function(startDate, endDate, value) {
        var data = dataHandler.RequestData(startDate, endDate, value);
    }
    
Une fois que la fonction a terminé son exécution, nous pouvons déclencher un événement qui permettra de retourner ``data`` au client.

.. captionup:: socketio.js
.. code-block:: js
    :emphasize-lines: 3

    socket.on("requestData", function(startDate, endDate, value) {
        var data = dataHandler.RequestData(startDate, endDate, value);
        socket.emit("returnData", data);
    }
    
Cet événement peut maintenant être récupéré dans ``graph.js``. Dans le prochain chapitre, il servira à lancer la génération du graphique. Pour l'instant, nous pouvons simplement afficher ``data`` pour être sûr que 
le processus se soit déroulé correctement.


.. captionup:: graph.js
.. code-block:: js

    socket.on("returnData", function(data) {
        console.log(data)
    }
    
Voilà ! Le client a maintenant récupéré les données qu'il avait demandées. Cependant, celles-ci ne sont pour l'instant que des données brutes. Nous allons nous charger dans le prochain chapitre de les transformer en de beaux graphiques.


.. [#f1] Code source pris de : http://stackoverflow.com/questions/143847/best-way-to-find-if-an-item-is-in-a-javascript-array